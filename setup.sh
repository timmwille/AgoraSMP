#!/bin/bash

if [[ "$EUID" -ne 0 ]]; then
  echo "Please run as root"
  exit 1
fi

echo "Welcome to the AgoraSMP setup tool!"
echo "This script will help you set up Agora Core."

function regex_escape() {
  echo -n "$1" | sed 's/[\@\/.,[\*^$()+?{|]/\\&/g'
}

SUGGESTED_VER="18.04"
RANDOM_PW_LENGTH=20 # length of auto-generated LDAP system user passwords

distro=$(lsb_release -is)
if [[ "$distro" == "Ubuntu" ]]; then
  ver=$(lsb_release -rs)
  if [[ "$ver" == "$SUGGESTED_VER" ]]; then
    echo "[+] Ubuntu $SUGGESTED_VER detected. You're using the recommended version."
  else
    echo "[!] You're installing on Ubuntu $ver. While this might work, we"
    echo "    recommend installing on Ubuntu $SUGGESTED_VER."
  fi
else
  echo "[!] You're installing on $distro. While this might work, Agora Core is"
  echo "    most tested on Ubuntu 18.04. Proceed on your own risk."
fi

extraApt=""
extraPip=""

read -p "Project name: " organization
read -p "Domain: " domain
read -p "Domain for Core: " coreDomain
read -p "Support mail address: " supportMail
read -p "Database [Postgres/sqlite]: " database # TODO verify
if [[ "$database" != "sqlite" ]]; then
  echo "PostgreSQL selected"
  read -p "Database host: " dbHost
  read -p "Database port: " dbPort
  read -p "Database name: " dbName
  read -p "Database user: " dbUser
  read -s -p "Database password: " dbPass
  echo
  databaseConf="'default':{'ENGINE':'django_prometheus.db.backends.postgresql','NAME':'$dbName','USER':'$dbUser','PASSWORD':'$dbPass','HOST':'$dbHost','PORT':'$dbPort'}"
  extraApt="libpq-dev"
  extraPip="psycopg2"
else
  databaseConf="'default':{'ENGINE':'django.db.backends.sqlite3','NAME':os.path.join(BASE_DIR,'db.sqlite3')}"
  installPostgresClient=0
fi
read -p "Server noreply mail DOMAIN: " outboundMailDomain
read -p "Mail host: " mailHost
read -p "Mail user: " mailUser
read -s -p "Mail password: " mailPass
echo
read -p "BigBlueButton API endpoint: " bbbUrl
read -s -p "BigBlueButton API secret: " bbbKey
echo
read -p "Static file root: " staticRoot
read -s -p "LDAP administrator password: " ldapadminpw
echo
read -s -p "Agora Core initial superuser password: " agoraadminpw
echo
read -p "Agora Core superuser email: " agoraadminmail

read -p "Start installation? [Y/n] " choice
test "$choice" != "n" || exit 0

echo "[+] Installing dependencies from APT"
cat << EOF | debconf-set-selections
slapd slapd/password1 password admin
slapd slapd/password2 password admin
slapd slapd/ppolicy_schema_needs_update select abort installation
slapd slapd/dump_database select when needed
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
slapd slapd/no_configuration boolean false
slapd slapd/move_old_database boolean true
slapd slapd/purge_database boolean false
slapd slapd/invalid_config boolean true
slapd shared/organization string $organization
slapd slapd/domain string $domain
EOF

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes --no-remove \
  build-essential pkg-config libldap2-dev libsasl2-dev libssl-dev libbz2-dev \
  python3-dev python3 python3-pip python3-venv redis slapd ldap-utils pwgen $extraApt

echo "[+] Installing Rust from Snap"
snap install rustup --classic
rustup install stable
rustup default stable

# password is "admin" for now, it's set to the user's choice once we're done
# (we need to pass it as a command parameter before and something else could
# read it from the process stats, so we set it afterwards)

usermgrpw=$(pwgen -n1 -s $RANDOM_PW_LENGTH)

echo "[+] Configuring OpenLDAP"
cat << EOF | ldapmodify -Q -Y EXTERNAL -H ldapi:///
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: memberof
olcModuleLoad: refint
EOF

cat << EOF | ldapadd -Q -Y EXTERNAL -H ldapi:///
dn: olcOverlay={0}memberof,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: top
olcOverlay: memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf

dn: olcOverlay={1}refint,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcRefintConfig
objectClass: top
olcOverlay: {1}refint
olcRefintAttribute: memberof member manager owner
EOF

echo "[+] Bootstrapping LDAP database"
dc="dc=${domain//\./,dc=}" # sic
admindn="cn=admin,$dc"
agoracryptpass=$(slappasswd -h {SSHA} -s ${agoraadminpw})
usermgrcryptpass=$(slappasswd -h {SSHA} -s ${usermgrpw})
cat << EOF | ldapadd -D $admindn -w admin
dn: ou=groups,$dc
objectClass: organizationalUnit
ou: groups

dn: ou=accounts,$dc
objectClass: organizationalUnit
ou: accounts

dn: ou=serviceAccounts,$dc
objectClass: organizationalUnit
ou: serviceAccounts

dn: uid=superuser,ou=accounts,$dc
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: inetOrgPerson
uid: superuser
cn: superuser
givenName: Super
sn: User
displayName: Super User
mail: $agoraadminmail
userPassword: ${agoracryptpass}

dn: cn=smp-active,ou=groups,$dc
objectClass: groupOfNames
cn: smp-active
member: uid=superuser,ou=accounts,$dc

dn: cn=smp-staff,ou=groups,$dc
objectClass: groupOfNames
cn: smp-staff
member: uid=superuser,ou=accounts,$dc

dn: cn=smp-admin,ou=groups,$dc
objectClass: groupOfNames
cn: smp-superuser
member: uid=superuser,ou=accounts,$dc

dn: uid=smp-usermgr,ou=serviceAccounts,$dc
objectClass: account
objectClass: simpleSecurityObject
objectClass: top
uid: smp-usermgr
userPassword: ${usermgrcryptpass}
EOF

echo "[+] Setting up LDAP permissions"

cat << EOF | ldapmodify -Q -Y EXTERNAL -H ldapi:///
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
EOF

cat << EOF | ldapmodify -Q -Y EXTERNAL -H ldapi:///
dn: olcDatabase={1}mdb,cn=config
add: olcAccess
olcAccess: {0}to attrs=userPassword by self write by dn="uid=smp-usermgr,ou=serviceAccounts,$dc" write by anonymous auth by * none
olcAccess: {1}to attrs=shadowLastChange by self write by * read
olcAccess: {2}to dn.subtree="ou=groups,$dc" by dn="uid=smp-usermgr,ou=serviceAccounts,$dc" write by * read
olcAccess: {3}to dn.subtree="ou=accounts,$dc" by dn="uid=smp-usermgr,ou=serviceAccounts,$dc" write by * read
olcAccess: {4}to * by * read
EOF

cat << EOF | ldapadd -Q -Y EXTERNAL -H ldapi:///
dn: cn=config
changetype: modify
add: olcDisallows
olcDisallows: bind_anon

dn: cn=config
changetype: modify
add: olcRequires
olcRequires: authc

dn: olcDatabase={-1}frontend,cn=config
changetype: modify
add: olcRequires
olcRequires: authc
EOF

echo "[+] Applying LDAP admin password"
ldapcryptpass=$(slappasswd -h {SSHA} -s ${ldapadminpw})
cat << EOF | ldapmodify -H ldapi:/// -x -D $admindn -w admin
dn: $admindn
changetype: modify
replace: userPassword
userPassword: ${ldapcryptpass}
EOF

echo "[+] Setting up system users"
adduser --system --group --home $(realpath .) agoracore
mkdir log
chown agoracore:agoracore log

echo "[+] Installing Agora Core"
python3 -m venv djangoenv
source djangoenv/bin/activate
pip3 install --upgrade pip
pip3 install wheel
pip3 install -r requirements.txt
if [[ "$extraPip" != "" ]]; then
  pip3 install $extraPip
fi

echo "[+] Building BBB API connector"
pip3 install maturin
cd pybbbapi
maturin develop --release
cd ..

echo "[+] Configuring Agora Core"
key=$(python manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())')
cd smpauth
cp local_settings.py.skeleton local_settings.py
perl -pi \
  -e "s/\\@\\@key/$(regex_escape $key)/;" \
  -e "s/\\@\\@supportMail/$(regex_escape $supportMail)/;" \
  -e "s/\\@\\@coreDomain/$(regex_escape $coreDomain)/;" \
  -e "s/\\@\\@database/$(regex_escape $databaseConf)/;" \
  -e "s/\\@\\@dc/$(regex_escape $dc)/;" \
  -e "s/\\@\\@usermgrpw/$(regex_escape $usermgrpw)/;" \
  -e "s/\\@\\@mailHost/$(regex_escape $mailHost)/;" \
  -e "s/\\@\\@mailUser/$(regex_escape $mailUser)/;" \
  -e "s/\\@\\@mailPass/$(regex_escape $mailPass)/;" \
  -e "s/\\@\\@outboundMailDomain/$(regex_escape $outboundMailDomain)/;" \
  -e "s/\\@\\@bbbUrl/$(regex_escape $bbbUrl)/;" \
  -e "s/\\@\\@bbbKey/$(regex_escape $bbbKey)/;" \
  -e "s/\\@\\@staticRoot/$(regex_escape $staticRoot)/;" \
  local_settings.py
cd ..

echo "[+] Initializing database"
python manage.py migrate --settings=smpauth.local_settings
echo "[+] Initializing static files"
python manage.py collectstatic --settings=smpauth.local_settings

deactivate # virtualenv

echo "[*] Setup completed"
echo "[*] Please check local_settings.py for any additional options you may like"
echo "[*] Afterwards, type './agora debug' to start your server in development"
echo "    mode. Please configure uWSGI instead for production use."
