# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.db import models
from common.constants import PositionOptions

from markdownx.models import MarkdownxField

import re

from usermgr.models import Seminar


class StaticPageManager(models.Manager):
    def for_seminar_view(self, seminar: Seminar):
        f = models.Q(seminar=None)
        if seminar is not None:
            f |= models.Q(seminar=seminar)
        return self.filter(f).exclude(enabled=False).order_by("title").distinct()


class StaticPage(models.Model):
    title = models.CharField(max_length=50)
    seminar = models.ForeignKey(
        Seminar, on_delete=models.CASCADE, blank=True, null=True
    )
    slug = models.SlugField(max_length=50, unique=True)

    position = models.CharField(
        choices=PositionOptions.choices, max_length=50, default=PositionOptions.HEADER
    )
    enabled = models.BooleanField(default=True)
    content = MarkdownxField(blank=True)
    extra_css = models.TextField(blank=True)

    objects = StaticPageManager()

    def __str__(self):
        return self.title

    @property
    def headings(self):
        text = self.content
        for match in re.finditer(
            "^(\#+) ([^\#{]+) {: \#([-\w_]+) }", text, re.MULTILINE
        ):
            level = len(match.group(1))
            title = match.group(2)
            tag = match.group(3)
            if level < 3:
                yield {"title": title, "tag": tag}
