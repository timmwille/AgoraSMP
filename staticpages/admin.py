# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.contrib import admin

from markdownx.admin import MarkdownxModelAdmin

from usermgr.admin_base import SeminarBoundModelMixin
from .models import StaticPage


@admin.register(StaticPage)
# class StaticPageAdmin(MarkdownxModelAdmin): # shit layout, fix, then re-enable
class StaticPageAdmin(SeminarBoundModelMixin, admin.ModelAdmin):
    list_display = ["title", "seminar", "enabled"]
    prepopulated_fields = {"slug": ("title",)}

    fieldsets = (
        (
            None,
            {"fields": ("title", "slug", "seminar", "position", "enabled", "content")},
        ),
        ("Styling", {"fields": ("extra_css",)}),
    )
