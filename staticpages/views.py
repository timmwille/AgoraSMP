# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

from common import site, constants

from .models import StaticPage


@login_required
def page(request, page_slug):
    p = get_object_or_404(StaticPage, slug=page_slug)
    nav = site.build_nav(request, constants.PositionOptions.HEADER)
    footer = site.build_nav(request, constants.PositionOptions.FOOTER)
    return render(
        request,
        "staticpages/page.html",
        {"page": p, "navbar_items": nav, "footer_items": footer},
    )
