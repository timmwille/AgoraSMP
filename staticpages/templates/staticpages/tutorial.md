{% comment %}
    Copyright (C) 2022 AgoraSMP Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
{% endcomment %}
# Tutorial

## BigBlueButton {: #bigbluebutton }
Via Big Blue Button we will conduct the lectures. You will find the link to the corresponding lecture room under the entry for the lecture in the schedule in your overview.

During the entire duration of the event, you will also have access to a video room for your working group as well as to the tea kitchen, which is open to all participants and presenters.

## Matrix {: #matrix }

### facility {: #facility }
#### Note: {: #note }
Matrix can in principle be used both in the browser and via a separate desktop application. The latter is often less complicated, since if you log in via the browser, you may have to provide your security key again for verification each time you log in. With the desktop version, this step is omitted.

#### Desktop version: {: #desktopversion }
* Download appropriate version from [element.io](https://element.io): [download for Windows](https://packages.riot.im/desktop/install/win32/x64/Element%20Setup.exe) / [download for macOS](https://packages.riot.im/desktop/install/macos/Element.dmg) / Linux instructions are on the start page of element.io.
* In the element window click on "Login".
* In the next window after "Log in to your Matrix account on matrix.org" click on "Change".
* Under home server address enter the Matrix server provided by your admin (typically [https://matrix.your-domain.com](https://matrix.your-domain.com)) and click on "Next".
* Log in with username and password from SmP platform, create security key and save it securely!
* Done!

#### Smartphone-App: {: #smartphone-app}
Matrix chat can also be used conveniently via app. The login instructions are the same as for the desktop version. Here you can find the app downloads:

* Android: [Element in Google Play Store](https://play.google.com/store/apps/details?id=im.vector.app&hl=de&gl=US)
* iOS: [Element Messenger in the Apple Appstore](https://apps.apple.com/de/app/element-messenger/id1083446067)
* Android (without Google services): [Element in F-Droid Store](https://f-droid.org/de/packages/im.vector.app/)

#### About Browser: {: #about-the-browser }
* Click on the "Matrix-Chat" app from the homepage.
* Click on "Login."
* Log in with username and password from SmP platform.
* Create a recovery key under Settings -> Privacy and save it securely!
* Done!

## Nextcloud {: #nextcloud }
For the seminar you will find a NextCloud among the applications on the homepage. There you can upload and download files and edit documents together. The login works with the same access data as on the rest of the SmP platform.