{% comment %}
    Copyright (C) 2022 AgoraSMP Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
{% endcomment %}
# Questions? Answers! {: #support }

If you have any questions, problems, suggestions or criticism, you can always contact the Orga-Team. The best way to reach us is in the matrix chat under the names below.

## Technical questions and problems with {{ BRAND }} {: #tech }
First point of contact:

* The [Support-Channel](matrix_link) on Matrix

For questions that you do not want to ask publicly, you can reach the organizers personally:

* [John](matrix_link)
* [Alice](matrix_link)
* ...

In case of technical difficulties you can reach the support team of {{ BRAND }}:

* [Bob](matrix_link)
* ...

## Questions about your working group {: #wg }
For questions directly related to your working group, please talk to the group moderators first.

## General questions about the process {: #common }
Feel free to ask them in the [Channel of the Seminar XYZ ](matrix_link) in Matrix.