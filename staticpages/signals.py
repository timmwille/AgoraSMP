# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.dispatch import receiver
from django.template import loader
from django.db import transaction
from staticpages.models import StaticPage
from usermgr.signals.signals import seminar_created
from common.constants import PositionOptions
from smpauth import settings

import logging

logger = logging.getLogger(__name__)


@receiver(seminar_created, dispatch_uid="staticpages")
def on_seminar_created(sender, **kwargs):
    seminar = kwargs["seminar"]
    logger.info('Setting up default static pages for seminar "%s"', seminar.slug)

    pages = []

    for template in getattr(settings, "STATICPAGE_TEMPLATES", []):
        try:
            pages.append(
                StaticPage(
                    title=template["title"],
                    slug=template["slug-prefix"] + "-" + seminar.slug,
                    seminar=seminar,
                    enabled=template.get("enabled", False),
                    position=template.get("position", PositionOptions.HEADER),
                    content=loader.render_to_string(
                        "staticpages/" + template["template"], {"BRAND": settings.BRAND}
                    ),
                )
            )
        except:
            logger.exception("Incorrect staticpage template")

    # required to not crash django signals if a database error occurs
    with transaction.atomic():
        for page in pages:
            page.save()

    logger.info("Finished setting up default static pages.")
