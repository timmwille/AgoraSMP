![AgoraSMP](./readme-images/agorasmp_logo_color.png)


AgoraSMP is an integrated **seminar management platform**. Participants of your online event will be able to use a variety of collaboration tools through a unified login, as well as a timetable and seminar and workshop group management.

AgoraSMP is modular and builds on top of **existing open-source software**. At the moment it has integrations for
* [Nextcloud](https://nextcloud.com/) ☁️ File sharing, Collaborative document editing, Forms
* [Matrix](https://matrix.org/) 💬 Chat
* [BigBlueButton](https://bigbluebutton.org/) 🖥️ Video chat, Conference rooms, Online presentations
* Coming soon: BigBlueButton based matchmaking (meet seminar participants shuffeld in a speed-dating like manner)

Since AgoraSMP manages user accounts and groups in an **LDAP** database, a wide range of further applications can be integrated with little to no effort. If you need more control, AgoraSMP provides a **Django-based API** through which you can perform more fine-grained provisioning and make your applications react to events on the platform.

AgoraSMP can run multiple seminars in parallel, each with their own set of administrators and per-workgroup moderators. This makes it easy to host a series of your own events, or even host for others!

## ✨ Features
* Frontpage 🚏
    * Interactive timetable for each seminar
    * Easy access to the relevant BigBlueButton meetings
    * Easy access to integrated apps (Files, Chat, ...)
    * Account settings
    * Self service sign-up for workgroups (optional)
    * Support page and other custom static pages
* Admin Panel 📋 (web interface) including the easy management of
    * Events
    * Seminars, with their own administrators and moderators
    * Workgroups
    * Announcements (on front page and automated via Matrix-chat)
    * Persistent meetings
    * Seminar user import (participants claim their accounts via e-mail)
* Django stats for Grafana

## 🏠 Architecture
![Architecture diagram](./readme-images/agorasmp-architecture.png)
* Django (Python all teh things!!1 🧹)
* User management & authentication
    * OpenLDAP
* Application web server
    * nginx
    * uwsgi
* Deployment
    * Ansible (currently in closed beta)


## ⌨️ Deployment
After hosting a series of online seminars ourselves, we have decided to open-source our code. While there are still some rough edges, we believe the software can provide value to people holding their online events in a seminar/workgroup based fashion. If you consider running AgoraSMP for your event, please contact us and we will be happy to help you get started with your setup. We do not offer hosting.

## 💬 Contact the devs
Feel free to open an issue to this repo, and [join our Matrix group](https://matrix.to/#/#agorasmp:tchncs.de)!

## 🧑‍💻 Hacking

Any help is appreciated! Feel free to have a look at open issues. Also if you would like to integrate another application, do not hesistate to ask for help! :)

`smpauth` is the main directory of the Django project. The different features, such as the frontpage `usermgr` and the BigBlueButton integration `bbbbutton` are realized as individual apps. If this is the first Django project you are working on, and all the directories look confusing, we recommend reading [the very excellent official Django tutorial](https://www.djangoproject.com/start/).

Before sending pull requests or committing to this repository, please format your code with [Black](https://black.readthedocs.io/en/stable/getting_started.html). It's a simple step that ensures a consistent code style and makes this codebase more accessible for new contributors.

## 📖️ Documentation
We started some documentation that should evolve (maybe into a wiki) with more people using AgoraSMP:

### [User Documentation](doc/User-Documentation.md)

### [Seminar Admin Documentation](doc/Seminar-Admin-Documentation.md)

### Deployment Documentation
* (todo)

### Development Documentation
* (todo)

---

This software is licensed under the [GNU Affero General Public License, Version 3](./LICENSE).
