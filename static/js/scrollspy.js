// Copyright (C) 2022 AgoraSMP Team
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
document.addEventListener("DOMContentLoaded", function() {
    const scroll_offset = document.querySelector('header').clientHeight+10;
    function on_scrollspy_click(e) {
        e.preventDefault();
        // correct scroll coordinates such that sticky header is respected
        // (otherwise the target heading ends up under the navigation header)
        let target = document.getElementById(e.target.href.split('#')[1]);
        if (target) {
            let top = target.getBoundingClientRect().top;
            window.scrollBy({ top: top - scroll_offset });
        }
    }
    document.querySelectorAll(".sidenav li a")
      .forEach(e => e.addEventListener("click", on_scrollspy_click));
});