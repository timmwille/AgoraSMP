# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""smpauth URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings

urlpatterns = [
    path("", include("usermgr.urls")),
    path("prometheus/", include("django_prometheus.urls")),
    path("admin/", admin.site.urls),
    path("bbbbutton/", include("bbbbutton.urls")),
    path("staticpage/", include("staticpages.urls")),
    path("markdownx/", include("markdownx.urls")),
]


if hasattr(settings, "DEBUG_TOOLBAR") and settings.DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns


admin.site.site_header = f"{settings.BRAND} Admin"
admin.site.site_title = f"{settings.BRAND} Admin"

handler404 = "usermgr.views.error404"
handler403 = "usermgr.views.error403"
handler500 = "usermgr.views.error500"
