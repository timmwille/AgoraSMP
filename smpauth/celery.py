import os

from celery import Celery
from kombu.serialization import register
from kombu.utils import json as _json
from django.db import models

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "smpauth.local_settings")

# helper to reduce Model instances to their primary key (ID) when serializing,
# to prevent passing full database objects to tasks
# (a task MUST re-fetch from the database, since there is no guarantee that
# the in-memory representation of the object is still valid when the task runs)
class ModelEncoder(_json.JSONEncoder):
    def default(self, m):
        if isinstance(m, models.Model):
            return m.pk
        else:
            return super().default(m)


register(
    "model_aware_json",
    lambda x: _json.dumps(x, cls=ModelEncoder),
    _json.loads,
    content_type="json",
    content_encoding="utf-8",
)

app = Celery("smpauth")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
