# User Documentation

## Table of Contents
1. [Claim your account](#claim-your-account)
2. [AgoraSMP's start page](#agorasmp-s-start-page)
3. [Your profile and settings](#your-profile-and-settings)
4. [Groups](#groups)
5. [Applications](#applications)
6. [Video conference rooms](#video-conference-rooms)
7. [FAQ](#faq)

## Claim your account
To participate in an online-event you have registered with an e-mail addressed to the organizers. This e-mail allows you to claim your account when entering AgoraSMP's main page for the first time (you should have received a link by the organizers):

![Screenshot of the claim page](Screenshot_claim_account_no_trademarks.png "Screenshot Claim Account")

Make sure you choose a strong (long) password, unique for AgoraSMP. Also make sure to write it down (on paper) or in a password-manager because it is used for all of AgoraSMP's diverse applications requiring a password.

## AgoraSMP's start page 
After claiming you account you will enter AgoraSMP for the first time! If you are in a hurry - do not worry. Right in the middle you can find the interactive timetable of your event. You can click on each day and see the announced talks and work groups including the linked rooms, chats or materials.
The start page is meant as a central point of your event (Therefore the name AgoraSMP: The *agora* was a central public space in ancient Greek city-states). All the important information are announced here and you can find the tools needed to communicate, share and much more to successfully participate.

![Screenshot of the start page](Screenshot_main-page.png "Screenshot main-page")

On the top of the site you can see on the left the tabs for the overview we are talking about, a tab with useful tutorials for the apps and a tab with information on how to get support in urgent cases.

## Your profile and settings
Click on your profile name on the top right of the start page to access you profile and settings.
Here you can change e.g. the name displayed in meetings or change you password. If possible in your event, you can choose to join or leave some groups.

## Groups
Depending on the organizers you will be part of one or several work-groups. Additionally you might have the possibility to crate and choose your own groups.
Each group will have their own chat and file-share automatically created. 

## Applications
One of AgoraSMP's main features is to provide a wide range of online applications to work efficiently together. Here are possible applications (they will be chosen by your event-team).
![Principle of AgoraSMP and its applications](AgoraSMP_applications.png "image of AgoraSMP and its applications")

## Video conference rooms
Your event will most likely have one or more persistent rooms for video-conferencing. You can access them right below your Apps. Sometimes your event-team will close rooms in order to test or prepare for a presentation. Once your event-team unlocks the room, you should get a notification from them. If you can still not enter the room, reload the start page until it is unlocked again.
The software running in the background is [BigBlueButton](https://bigbluebutton.org/).

## FAQ

### 1. I want to join another work group. How do I do that?
This is only possible if the organizers enabled the participants to do so. You can check if it is possible by clicking on the top right onto your name, followed by 'manage project groups'. Here you can see the groups that can be joined. In case there are none you can finally ask the organizers.

### 2. My current browser is having difficulties with the video meeting. What do I need to change?
In general we recommend connecting via a PC and not via smartphone. It should also work in many cases with a smartphone but you will have less possibilities to work with the provided tools. 
The browsers known to work best with the video meeting are Firefox and Chromium/Chrome and for iOS Safari. 

(work in progress - information from:
https://www.bigbluebutton-hosting.de/hilfe/faq/#BigBlueButton-%C3%9Cbertragungs-Probleme
)

### 3. How do I create breakout sessions and new chat rooms?
To create a breakout session you need to be moderator (only in the video-meeting). You can recognize your status in the meeting on the left side next to your name. A round symbol means you do not have moderator rights and a square symbol indicates you are a moderator. If you are not, you can simply ask one of the existing moderators to upgrade your rights by left-clicking on you and selecting the option to promote you.
Once you are moderator you see a 'settings' symbol above the list of the participants were you can select to open breakout-rooms.
