# Seminar Admin Documentation

...page under construction...


Claim account like a normal user and ask your admin to upgrade you to moderator


## Table of Contents
1. [Admin Panel](#admin-panel)
2. [Dashboard](#dashboard)
3. [User](#user)
4. [Persistent meetings](#persistent-meetings)
5. [Matchmaking Sessions](#matchmaking-sessions)
6. [Seminars](#seminars)
7. [Announcements](#announcements)
8. [Events](#events)
9. [Workgroups](#workgroups)
1. [FAQ](#faq)


## Admin Panel

- user/leave_admin_panel -> start page
- Demo-Seminar to play around

![Admin panel](Screenshot_AdminPanel_AgoraCore.png "image of AgoraSMP Admin panel")


## Dashboard
- static pages (Tutorials, Support)

## User (Benutzer)

## Persistent meetings

- Persistent meetings are displayed on the start-page (either for everybody/seminar or for specific workgroups- not both!)
- [x] enable to allow users to join the meeting (otherwise it will not be shown)
- limitied persistent Meetings: Sometimes (briefing of the speackers etc.) you want to join first with the moderators and not allow the users to joine -> permissions - > privileged users
- bspw für unseren morgendlichen checkin?    oder für Briefings der Dozierende? permissions -> users are mod (moderators) -> e.g. for social rooms


## Matchmaking Sessions:

Feature is coming soon...

## Seminars:
- Several events can be planned and played independently 
- Description of the seminar goes here

## Announcements
- They pop up on the start page for every user or for explicit workgroups
- Can be clicked away from the users
- If you want to re-announce go into the specific announcement and save again

## Events = Time-table events
- [x] disable if you want to prepare in advance
- timeslot
- group-management (everybody or work-group - not both!)
- Interactive:  event -> add meeting! / add link (files like presentation foils from nextcloud etc.) / add polls (url)

## Workgroups:
- Click on workgroup / or add workgroup
- here you can change and add moderators for the specific workgroup
- Switch members (this defines the access to the groups, folders etc. in the apps) 
- [x] users can join -> user can choose to join the workgroups in their profile settings - click on save (!?)

Atention: Only asign eather a seminar or a workgroup - never both together (the result would not be as you might expect)


## FAQs
- Wie stellen wir Ordner für Kleingruppen zur Verfügung? --> Ordner in AG-Ordnern am praktikabelsten?!

Achtung: eindeutige Bennenung (sodass bsp Ordner im Nextcloud eindeutig sind): bspw. "Plenum digitale Akademie 2021"
