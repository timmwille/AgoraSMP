import importlib
import logging
from functools import partial

from django.db import transaction
from django.conf import settings
from django.dispatch import Signal

logger = logging.getLogger(__name__)


def try_schedule(task, kwargs):
    # we need to ensure that we don't throw exceptions in
    # transaction.on_commit since that would discard all further
    # callbacks
    try:
        task.apply_async([], kwargs, ignore_result=True)
    except:
        logger.exception("Failed to schedule Celery task %s", task.name)


def schedule_celery_job(task, **kwargs):
    # forward the signal
    # this operates in fire-and-forget mode; Celery takes care
    # of retrying on failure, so don't clutter Redis with the
    # task states that we'll never retrieve

    # arguments coming in from Signal that we don't need
    kwargs.pop("signal")
    kwargs.pop("sender")

    # ensure that the task is only queued once the DB transaction
    # has completed, otherwise the DB entries might not be visible
    # to Celery (if we're not in a transaction, the callback will
    # fire immediately as per Django documentation, so an explicit
    # check is not needed here)
    transaction.on_commit(lambda: try_schedule(task, kwargs))


def filter_synthetic(task, expected_synthetic, **kwargs):
    if kwargs.get("synthetic") == expected_synthetic:
        # remove the argument, such that the celery job doesn't have
        # to add this as a keyword argument to its signature (which would
        # be useless, since the value would always be expected_synthetic)
        kwargs.pop("synthetic")
        schedule_celery_job(task, **kwargs)


SIGNAL_REGISTRY = dict()


def discover_signals():
    signal_paths = settings.INSTALLED_SIGNAL_MODULES
    for p in signal_paths:
        # load module and discover Django all signals defined there
        module = importlib.import_module(p)
        signals = {
            name: sig
            for name in dir(module)
            if type(sig := getattr(module, name)) is Signal
        }

        # ensure the signal names don't contain underscores (this is a reserved delimiter
        # that we need for ourselves)
        illegal_signals = [p + "." + k for k in signals.keys() if "__" in k]
        if illegal_signals:
            raise ValueError(
                "Signal names exposed to celery must not contain double underscore:"
                + str(illegal_signals)
            )

        # ensure we don't have any naming conflicts across modules
        dups = SIGNAL_REGISTRY.keys() & signals.keys()
        if dups:
            collisions = []
            for d in dups:
                collisions.append((p + "." + d, SIGNAL_REGISTRY[d].__module__ + d))
            raise ValueError("Signal name collision:" + str(collisions))

        # register the discovered signals for exposing to Celery
        SIGNAL_REGISTRY.update(signals)


def bind_celery_handlers():
    worker_paths = settings.INSTALLED_WORKERS
    for p in worker_paths:
        # load "tasks" module from worker and discover all handlers (functions starting with "handle_")
        module = importlib.import_module(p + ".tasks")
        handlers = {
            name[7:]: getattr(module, name)
            for name in dir(module)
            if name.startswith("handle_")
        }

        for name, fun in handlers.items():
            # look up the exposed Django signal from our registry
            signal_name = name.split("__", 1)[0]
            signal = SIGNAL_REGISTRY.get(signal_name)
            if not signal:
                logger.info("Worker %s references undefined signal %s", p, signal_name)
                continue

            # connect the signal to the current handler
            if name.endswith("__nonsynthetic"):
                signal.connect(partial(filter_synthetic, fun, False), weak=False)
            elif name.endswith("__synthetic"):
                signal.connect(partial(filter_synthetic, fun, True), weak=False)
            else:
                signal.connect(partial(schedule_celery_job, fun), weak=False)
            logger.debug("Connected worker %s to signal %s", p, signal_name)
