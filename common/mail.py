# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings


def email_render(template_file, context_dict):
    if template_file[-4:] != ".txt":
        template_file += ".txt"
    rendered = render_to_string(template_file, context=context_dict).split("\n", 2)
    subject = rendered[0]
    body = rendered[2]
    return {"subject": subject, "body": body}


def send_templated_mail(template_file, emails, context_dict):
    email = email_render("mail/" + template_file, context_dict)
    send_mail(email["subject"], email["body"], settings.DEFAULT_FROM_EMAIL, emails)
