# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic.base import TemplateView

from common.constants import PositionOptions
from staticpages.models import StaticPage
from usermgr.models import Seminar

## all functions needed to control the main template

# TODO some of these could be refactored into template tags

# template context processor, makes BRAND variable available in templates
def branding(request):
    return {"BRAND": settings.BRAND}


def build_nav(request, position=PositionOptions.HEADER):
    seminar_slug = request.session.get("selected_seminar")
    # this basically does get(), but returns None instead of throwing DoesNotExist
    seminar = Seminar.objects.filter(slug=seminar_slug).first()
    nav_items = []
    if request.user.is_authenticated:
        if position == PositionOptions.HEADER:
            nav_items.append({"name": "index", "title": "My Overview"})
        for p in StaticPage.objects.for_seminar_view(seminar):
            if p.position == position:
                nav_items.append(
                    {
                        "name": "staticpage",
                        "view_kwargs": {"page_slug": p.slug},
                        "title": p.title,
                    }
                )
    current = request.resolver_match
    for itm in nav_items:
        itm["url"] = reverse(itm["name"], kwargs=itm.get("view_kwargs", {}))
        itm["active"] = (
            itm["name"] == current.url_name
            and itm.get("view_kwargs", {}) == current.kwargs
        )  # auto-detect the active page
    return nav_items


class FormContext:
    """
    Wrapper class around forms that provides a common way to construct it,
    load its initial data, and perform some action when a valid form is
    submitted.
    This class is used to plug forms into MultiformView.
    """

    def construct_form(self, request, post, initial, prefix):
        raise

    def get_initial_data(self, request):
        return {}

    def form_action(self, request, form):
        return True


class MultiformView(TemplateView):
    """
    A view that you can plug multiple forms into (using FormContext).
    """

    redirect_url = None
    form_type_field = None
    form_contexts = dict()
    form_insts = dict()

    def response_invalid_form_type(self, request, *args, **kwargs):
        messages.error(request, "Invalid form type")
        return super().get(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        # find out if a form has been submitted, and if so, which one
        ftype = request.POST.get(self.form_type_field)
        # initialize all forms
        for f, form_ctx in self.form_contexts.items():
            self.form_insts[f] = form_ctx.construct_form(
                request,
                request.POST if ftype == f else None,
                None if ftype == f else form_ctx.get_initial_data(request),
                f,
            )
        # maybe run the action of the submitted form
        if ftype is not None:
            submitted_form = self.form_insts.get(ftype)
            if submitted_form is None:
                return self.response_invalid_form_type(request, *args, **kwargs)
            if submitted_form.is_bound and submitted_form.is_valid():
                if self.form_contexts[ftype].form_action(request, submitted_form):
                    # if action was successful, send redirect to client
                    # to prevent re-submission if the user reloads the page
                    return redirect(self.redirect_url)
        # render response (force use of 'get' since TemplateView doesn't handle
        # POST automatically)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # push all forms into the template context
        for f, inst in self.form_insts.items():
            context[f] = inst
        return context
