# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import get_objects_for_user
from django.contrib.auth import get_permission_codename
from django.contrib.admin.options import BaseModelAdmin, InlineModelAdmin
from django.db.models import ForeignKey

# updated from https://stackoverflow.com/a/11434009/3697444
class PerItemPermsModelMixin(BaseModelAdmin):

    relax_reference_permissions = []

    def get_queryset(self, request):
        qs = super(PerItemPermsModelMixin, self).get_queryset(request)
        if super(PerItemPermsModelMixin, self).has_view_permission(request) or super(
            PerItemPermsModelMixin, self
        ).has_change_permission(request):
            return qs
        perms = [get_permission_codename("change", self.opts)]
        # InlineModelAdmin doesn't have that field
        if hasattr(self, "list_editable") and not self.list_editable:
            # in this case change implies view, so accept both
            perms.append(get_permission_codename("view", self.opts))
        return get_objects_for_user(request.user, perms, qs, any_perm=True)

    def has_change_permission(self, request, obj=None):
        # never called on the list page
        if super(PerItemPermsModelMixin, self).has_change_permission(request, obj):
            return True
        return request.user.has_perm(get_permission_codename("change", self.opts), obj)

    def has_view_permission(self, request, obj=None):
        if super(PerItemPermsModelMixin, self).has_view_permission(request, obj):
            return True
        if obj is not None:  # we're asked on an element page
            return request.user.has_perm(
                get_permission_codename("view", self.opts), obj
            ) or self.has_change_permission(request, obj)
        # never call get_queryset on an InlineModelAdmin (this will cause a recursion
        # error since that would itself call has_view_permission)
        if isinstance(self, InlineModelAdmin):
            return False
        # we're asked on a list page and user doesn't have global permission, so check
        # if they can view _any_ element
        return self.get_queryset(request).exists()

    def has_delete_permission(self, request, obj=None):
        # never called on the list page
        if super(PerItemPermsModelMixin, self).has_delete_permission(request, obj):
            return True
        return request.user.has_perm(get_permission_codename("delete", self.opts), obj)

    def has_module_permission(self, request):
        if super().has_module_permission(request):
            return True
        # show the app in the admin navigation if the user can interact
        # with any model
        return self.has_view_permission(request, obj=None) or self.has_add_permission(
            request
        )

    def restrict_referenced_key(self, model, foreign, request, gen_kwargs, relax=None):
        foreign_model = model._meta.get_field(foreign).related_model
        if relax is None:
            relax = foreign_model in self.relax_reference_permissions
        foreign_opts = foreign_model._meta
        fk_viewperm = get_permission_codename("view", foreign_opts)
        fk_changeperm = get_permission_codename("change", foreign_opts)
        fk_perms = [fk_changeperm]
        if relax:
            fk_perms.append(fk_viewperm)
        # if the user doesn't have global permission to view this target type, then only
        # show the entries that they have object-based permission to using django-guardian
        if not request.user.has_perm(fk_changeperm) and not (
            relax and request.user.has_perm(fk_viewperm)
        ):
            if "queryset" in gen_kwargs:
                queryset = gen_kwargs["queryset"]
            else:
                queryset = foreign_model.objects.get_queryset()
            gen_kwargs["queryset"] = get_objects_for_user(
                request.user, fk_perms, queryset, any_perm=True
            )
            return True
        return False

    # make sure users can't peek at objects through foreign-key dropdowns if they don't have
    # view permissions on that object (we disable single-choice fields entirely and hide
    # protected choices from multi-select fields)

    def get_readonly_fields(self, request, obj=None):
        readonly = []
        # skips if this is called for any of our InlineAdmins
        if obj and obj._meta.model == self.model:
            for field in filter(lambda f: isinstance(f, ForeignKey), obj._meta.fields):
                kwargs = dict()
                self.restrict_referenced_key(field.model, field.name, request, kwargs)
                if "queryset" not in kwargs:  # key is not restricted
                    continue
                orig = getattr(obj, field.name)
                if (
                    orig is not None
                    and not kwargs["queryset"].filter(id=orig.id).exists()
                ):
                    # you may not edit a field if you can't see its current key
                    readonly.append(field.name)
                    # FIXME SECURITYBUG You can still _see_ the name of the thing you're not allowed to edit
        return readonly

    # NOTE
    # This doesn't allow seminar admins to see their workgroups in a dropdown, if they don't have
    # explicit permission for that workgroup. That breaks adding persistent meetings to workgroups.
    # The solution is to add UsingSeminarBoundFieldMixin, since Workgroups are also seminar bound things.

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        self.restrict_referenced_key(db_field.model, db_field.name, request, kwargs)
        return super(PerItemPermsModelMixin, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        was_restricted = self.restrict_referenced_key(
            db_field.model, db_field.name, request, kwargs
        )
        field = super(PerItemPermsModelMixin, self).formfield_for_manytomany(
            db_field, request, **kwargs
        )
        if was_restricted:
            field.help_text = "Some entries may have been hidden, since you do not have the required permission to view them."
        return field

    def save_model(self, request, obj, form, change):
        if change:  # we don't care about add
            # inject restricted and therefore not shown referenced objects back into the form before saving
            for field in obj._meta.many_to_many:
                if field.name not in form.fields:
                    continue
                kwargs = dict()
                self.restrict_referenced_key(field.model, field.name, request, kwargs)
                if "queryset" not in kwargs:  # key is not restricted
                    continue
                formfield = form.fields[field.name]
                # HACK we can't append to querysets easily, so we use a python list
                # There's still no proper Django way to do m2m validation unless you accept
                # saving twice (which will trigger signals twice, which is unacceptable here)
                orig = getattr(obj, field.name).get_queryset()
                invisible_entries = list(orig.difference(kwargs["queryset"]))
                form.cleaned_data[field.name] = (
                    list(form.cleaned_data[field.name]) + invisible_entries
                )
        super(PerItemPermsModelMixin, self).save_model(request, obj, form, change)
