# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import annotations

from django.db import models
from django.db.models import CheckConstraint, Q, F
from django.utils import timezone
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from itertools import chain

import hashlib

from bbbbutton.models import PersistentMeeting
from common.security import is_safe_url

import logging

logger = logging.getLogger(__name__)


def make_seminar_bound_filter(seminar, workgroups):
    return (
        Q(seminar=seminar)
        | Q(workgroups__in=workgroups)
        | (Q(seminar=None) & Q(workgroups=None))
    )


class Application(models.Model):
    # a Satellite Service (Nextcloud, Matrix, Mumble, etc.) connected
    # to this Agora instance
    name = models.CharField(max_length=50)
    app_menu_url = models.URLField()
    # "maintenance mode" switch
    available = models.BooleanField(default=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class SeminarManager(models.Manager):
    def all_with_pending_events(self):
        now = timezone.now()
        # return all seminars that are not yet in the state they should be in
        return self.filter(
            (Q(state=Seminar.States.CREATED) & Q(access_opens__lt=now))
            | (Q(state=Seminar.States.OPENED) & Q(start_date__lt=now))
            | (Q(state=Seminar.States.RUNNING) & Q(end_date__lt=now))
            | (Q(state=Seminar.States.COMPLETED) & Q(access_closes__lt=now))
            # TODO maybe also add a "destroy at" time
        )

    def all_accessible(self):
        now = timezone.now()
        return self.filter(access_opens__lt=now, access_closes__gt=now)

    def all_running(self):
        now = timezone.now()
        return self.filter(start_date__lt=now, end_date__gt=now)

    def all_not_ended(self):
        now = timezone.now()
        return self.filter(end_date__gt=now)

    def with_member(self, member: Profile, accessible=False, future=False):
        assert not (accessible and future)
        # DO NOT use this:
        # q = self.filter(workgroup__in=member.workgroups.all())

        # It will cause q to contain the same seminar twice if there is more than
        # one if its workgroups in member.workgroups. (And don't try to use distinct()
        # to fix this -- after distinct, there are many methods that become forbidden,
        # which would cause weird errors for our callers!)
        q = self.filter(
            id__in=member.workgroups.values_list("seminar", flat=True).distinct()
        )
        if accessible:
            now = timezone.now()
            q = q.filter(access_opens__lt=now, access_closes__gt=now)
        if future:
            now = timezone.now()
            q = q.filter(access_opens__gt=now)
        return q


class Seminar(models.Model):
    class States(models.TextChoices):
        CREATED = "Created"
        OPENED = "Opened"
        RUNNING = "Running"
        COMPLETED = "Completed"
        CLOSED = "Closed"
        DESTROYED = "Destroyed"

    # a seminar, such as "SmP on Web Development"
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=30, unique=True)
    description = models.TextField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    access_opens = models.DateTimeField()
    access_closes = models.DateTimeField()
    state = models.CharField(choices=States.choices, max_length=10)
    admins = models.ManyToManyField(User, blank=True)
    applications = models.ManyToManyField(Application, blank=True)

    objects = SeminarManager()

    class Meta:
        constraints = [
            # check that the dates are sensible
            CheckConstraint(
                check=Q(access_opens__lte=F("start_date")),
                name="check_start_not_before_access_opens",
            ),
            CheckConstraint(
                check=Q(start_date__lt=F("end_date")), name="check_start_end_order"
            ),
            CheckConstraint(
                check=Q(end_date__lte=F("access_closes")),
                name="check_access_closes_not_before_end",
            ),
        ]

    def __str__(self):
        return (
            self.name
            + " ("
            + timezone.localtime(self.start_date).strftime(settings.SEMINAR_DATEFORMAT)
            + " - "
            + timezone.localtime(self.end_date).strftime(settings.SEMINAR_DATEFORMAT)
            + ")"
        )

    # TODO make these depend on the "state" field instead

    def is_active(self):
        now = timezone.now()
        return self.start_date <= now <= self.end_date

    def is_access_open(self):
        now = timezone.now()
        return self.access_opens <= now <= self.access_closes

    def is_not_yet_open(self):
        return self.access_opens > timezone.now()


class Workgroup(models.Model):
    # a work group in a seminar (a seminar must have at least one)
    # bound to a seminar and dies with it
    seminar = models.ForeignKey(Seminar, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=30, unique=True)
    description = models.TextField()
    moderators = models.ManyToManyField(User, blank=True)
    users_can_join = models.BooleanField(default=False)
    participant_limit = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name

    def get_member_count(self):
        return self.profile_set.count()

    @property
    def size_limited(self):
        return self.participant_limit > 0

    def is_full(self):
        return self.size_limited and (self.get_member_count() >= self.participant_limit)


class ExpectedParticipant(models.Model):
    # all participants that have been imported from an event's participant list
    # but that did not yet register an account
    email = models.EmailField(unique=True)
    given_name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    username = models.CharField(max_length=101)
    time_imported = models.DateTimeField(auto_now_add=True)
    workgroups = models.ManyToManyField(Workgroup, blank=True)
    registered = models.BooleanField(default=False)

    def __str__(self):
        return (
            self.given_name + " " + self.surname + (" [reg]" if self.registered else "")
        )


class TokenManager(models.Manager):
    def all_invalid(self):
        return self.filter(expires__lt=timezone.now())


class Token(models.Model):
    token = models.CharField(max_length=30, unique=True)
    issued = models.DateTimeField()
    expires = models.DateTimeField()
    objects = TokenManager()

    class Meta:
        abstract = True

    def is_valid(self):
        return self.expires < timezone.now()


class Claimtoken(Token):
    # a token sent via e-mail to a user to claim an account
    participant = models.ForeignKey(ExpectedParticipant, on_delete=models.CASCADE)


class Mailtoken(Token):
    # a token sent via e-mail to a user to confirm their new email address
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    new_email = models.EmailField()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    workgroups = models.ManyToManyField(Workgroup, blank=True)

    def __str__(self):
        return self.user.__str__()  # delegate


# autogenerate a profile for each newly registered user
@receiver(post_save, sender=User)
def handle_new_user(sender, **kwargs):
    user = kwargs.get("instance")
    if not hasattr(user, "profile"):
        logger.info("POST_SAVE : Auto-creating profile for user : %s" % user)
        p = Profile(user=user)
        p.save()


class DeletedUserManager(models.Manager):
    def account_existed(self, username):
        sha3inst = hashlib.sha3_256()
        sha3inst.update(username.encode("utf-8"))
        h = sha3inst.hexdigest()
        return self.get_queryset().filter(name_hash=h).exists()


class DeletedUser(models.Model):
    name_hash = models.CharField(max_length=256, unique=True)
    deleted_date = models.DateTimeField(default=timezone.now)

    objects = DeletedUserManager()

    @staticmethod
    def from_username(username):
        sha3inst = hashlib.sha3_256()
        sha3inst.update(username.encode("utf-8"))
        h = sha3inst.hexdigest()
        return DeletedUser(name_hash=h)


class EventManager(models.Manager):
    def for_seminar_view(self, seminar: Seminar, workgroups):
        return (
            self.filter(make_seminar_bound_filter(seminar, workgroups))
            .exclude(disabled=True)
            .order_by("start_date")
            .distinct()
        )


# TODO refactor the event models and logic into new app
# to prevent the cyclic dependency between usermgr and bbbbutton
# (since Event needs PersistentMeeting and PersistentMeeting imports Seminar)
class Event(models.Model):
    # an event within a seminar, to be shown in the event schedule
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    disabled = models.BooleanField(default=False)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    workgroups = models.ManyToManyField(Workgroup, blank=True)
    # set this to some non-null value if you want to target the entire seminar
    # (note that not both workgroups and seminar can be null at the same time
    # and that not both can be non-null at the same time)
    seminar = models.ForeignKey(
        Seminar, on_delete=models.CASCADE, blank=True, null=True
    )
    # FIXME you can currently select a meeting that is not in this seminar or workshop
    meeting = models.ForeignKey(
        PersistentMeeting, on_delete=models.SET_NULL, blank=True, null=True
    )
    announced = models.BooleanField(default=False)

    objects = EventManager()

    # FIXME this causes migration to fail with 'ProgrammingError: column "workgroup_id" does not exist'
    class Meta:
        constraints = [
            #    # make sure that we have either a seminar or workgroups set, but not both
            #    CheckConstraint(check=(Q(seminar__isnull=True) & Q(workgroups=None)), name="event_has_seminar_not_workgroups"),
            #    CheckConstraint(check=(Q(seminar__isnull=False) & Q(workgroups=None)), name="event_has_workgroups_not_seminar"),
            CheckConstraint(
                check=Q(start_date__lt=F("end_date")),
                name="event_check_start_end_order",
            )
        ]

    def __str__(self):
        return (
            self.name + " (" + str(self.start_date) + " - " + str(self.end_date) + ")"
        )

    def contains_date(self, date):
        return self.start_date.date() <= date <= self.end_date.date()

    def contains_datetiime(self, datetime):
        return self.start_date <= datetime < self.end_date


class EventLink(models.Model):
    name = models.CharField(max_length=100)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    disabled = models.BooleanField(default=False)
    url = models.URLField()
    new_tab = models.BooleanField(default=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    show_disclaimer = models.BooleanField(default=True)

    class Meta:
        constraints = [
            CheckConstraint(
                check=Q(start_date__lt=F("end_date")),
                name="eventlink_check_start_end_order",
            )
        ]
        permissions = [
            (
                "hide_eventlink_disclaimer",
                "Can disable the disclaimer on outgoing links",
            ),
        ]

    def __str__(self):
        return self.name

    def is_available(self):
        return not self.disabled and (self.start_date <= timezone.now() < self.end_date)

    def is_future(self):
        return not self.disabled and self.start_date > timezone.now()

    def should_show_disclaimer(self):
        if not self.show_disclaimer:
            return False
        return not is_safe_url(self.url)


class AnnouncementManager(models.Manager):
    def for_seminar_view(self, user: User, seminar: Seminar, workgroups):
        now = timezone.now()
        return (
            self.filter(make_seminar_bound_filter(seminar, workgroups))
            .filter(issue_date__lte=now)
            .exclude(dismissed_by=user)
            .order_by("-issue_date")
            .distinct()
        )


class Announcement(models.Model):
    # an announcement within a seminar, to be shown above the event schedule
    name = models.CharField(max_length=100)
    content = models.TextField(blank=True)
    issue_date = models.DateTimeField(default=timezone.now)
    workgroups = models.ManyToManyField(Workgroup, blank=True)
    # set this to some non-null value if you want to target the entire seminar
    # (note that not both workgroups and seminar can be null at the same time
    # and that not both can be non-null at the same time)
    seminar = models.ForeignKey(
        Seminar, on_delete=models.CASCADE, blank=True, null=True
    )
    dismissed_by = models.ManyToManyField(User, blank=True)

    objects = AnnouncementManager()

    class Meta:
        ordering = ["-issue_date"]

    @property
    def dismissed_count(self):
        return self.dismissed_by.count()

    def __str__(self):
        return self.name
