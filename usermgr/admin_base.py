# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.contrib import admin
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.forms import ModelForm
from guardian.shortcuts import get_objects_for_user
from django.utils.translation import gettext as _
from common.guardian_extensions import PerItemPermsModelMixin

from .models import Seminar, Workgroup, Profile

#### HELPERS ####


class SeminarBoundAdminBase(admin.options.BaseModelAdmin):
    # Use this only as an abstract class.
    # Contains some goodies to make implementing the various seminar bound admin mixins easier.

    def get_viewable_seminars(self, request):
        return get_objects_for_user(
            request.user,
            ["view_seminar", "change_seminar"],
            klass=Seminar,
            any_perm=True,
        )

    def get_viewable_workgroups(self, request):
        direct = get_objects_for_user(
            request.user,
            ["view_workgroup", "change_workgroup"],
            klass=Workgroup,
            any_perm=True,
        )
        seminars = self.get_changeable_seminars(request)
        # you can implicitly VIEW a workgroup if you can EDIT its seminar
        indirect = Workgroup.objects.filter(
            id__in=seminars.values_list("workgroup", flat=True)
        )
        return (direct | indirect).distinct()

    def get_changeable_seminars(self, request):
        return get_objects_for_user(request.user, ["change_seminar"], klass=Seminar)

    def get_changeable_workgroups(self, request):
        direct = get_objects_for_user(
            request.user, ["change_workgroup"], klass=Workgroup
        )
        seminars = self.get_changeable_seminars(request)
        # you can implicitly edit a workgroup if you can edit its seminar
        indirect = Workgroup.objects.filter(
            id__in=seminars.values_list("workgroup", flat=True)
        )
        return (direct | indirect).distinct()

    # WARNING: The output of this filter is not distinct (which is a necessary evil, since
    # sometimes you need to do some other transformation before applying distinct())
    def make_implicit_query_filter(
        self,
        request,
        use_seminar=False,
        use_workgroup=False,
        use_workgroups=False,
        filter_prefix="",
        relax=False,
        allow_unbound=True,
    ):
        unbound_filter = dict()
        query = None
        workgroup_filter = None
        # can be a one-to-many or many-to-many relation
        if use_workgroups:
            workgroup_filter = {
                filter_prefix + "workgroups__in": self.get_viewable_workgroups(request)
                if relax
                else self.get_changeable_workgroups(request)
            }
            unbound_filter.update({filter_prefix + "workgroups__isnull": True})
        elif use_workgroup:
            workgroup_filter = {
                filter_prefix + "workgroup__in": self.get_viewable_workgroups(request)
                if relax
                else self.get_changeable_workgroups(request)
            }
            unbound_filter.update({filter_prefix + "workgroup__isnull": True})
        if workgroup_filter is not None:
            query = Q(**workgroup_filter)
        if use_seminar:
            seminar_filter = {
                filter_prefix + "seminar__in": self.get_viewable_seminars(request)
                if relax
                else self.get_changeable_seminars(request)
            }
            if query is not None:
                query |= Q(**seminar_filter)
            else:
                query = Q(**seminar_filter)
            unbound_filter.update({filter_prefix + "seminar__isnull": True})
        if query is not None and allow_unbound:
            query |= Q(**unbound_filter)
        return query


class SeminarBoundProtectedModelForm(ModelForm):

    allow_global_object = False

    def clean(self):
        cleaned_data = super().clean()
        if self.allow_global_object:
            return cleaned_data
        # early exit if we're not modifying any binding information (e.g. when the fields are all
        # read-only, then they're not in cleaned_data
        if (
            "workgroups" not in cleaned_data
            and "workgroup" not in cleaned_data
            and "seminar" not in cleaned_data
        ):
            return cleaned_data
        if (
            not self.allow_global_object
            and not cleaned_data.get("workgroups", Workgroup.objects.none()).exists()
            and cleaned_data.get("workgroup") is None
            and cleaned_data.get("seminar") is None
        ):
            raise ValidationError(
                _("You do not have permission to create global %(objname)s."),
                params={"objname": self.instance._meta.verbose_name_plural},
                code="noglobal",
            )
        return cleaned_data


class UsingSeminarBoundFieldMixin(SeminarBoundAdminBase):

    # widens the referenced key restriction to allow viewing things that are bound to a
    # viewable seminar or workgroup

    def get_implicit_queryset_for_foreign(self, request, foreign_model):
        if foreign_model is User:
            # User objects need special handling, since they are bound to workgroups and seminars
            # through their Profile related model, but not directly. (This special handling is
            # essentially the equivalent for fields to what the 'ref' attribute on
            # SeminarBoundModelMixin does for the list views.)
            query = self.make_implicit_query_filter(
                request,
                use_workgroups=True,
                filter_prefix="profile__",
                relax=False,
                allow_unbound=False,
            )
        else:
            workgroups_field_exists = hasattr(foreign_model, "workgroups")
            workgroup_field_exists = hasattr(foreign_model, "workgroup")
            seminar_field_exists = hasattr(foreign_model, "seminar")
            query = self.make_implicit_query_filter(
                request,
                use_seminar=seminar_field_exists,
                use_workgroup=workgroup_field_exists,
                use_workgroups=workgroups_field_exists,
                relax=False,
            )
        if query is not None:
            return foreign_model.objects.filter(query)
        return foreign_model.objects.none()

    def restrict_referenced_key(self, model, foreign, request, gen_kwargs, relax=None):
        was_restricted = super(
            UsingSeminarBoundFieldMixin, self
        ).restrict_referenced_key(model, foreign, request, gen_kwargs, relax)
        # TODO refactor the models and extract a common SeminarBoundModel that we can
        # then do instance checking on
        foreign_model = model._meta.get_field(foreign).related_model
        # if not restricted we can see everything anyway, no need to add something
        if was_restricted:
            implicit = self.get_implicit_queryset_for_foreign(request, foreign_model)
            gen_kwargs["queryset"] = (gen_kwargs["queryset"] | implicit).distinct()
        return was_restricted


class SeminarBoundModelMixin(PerItemPermsModelMixin, SeminarBoundAdminBase):
    # - if a user has view permission on a seminar, they can see all items bound to
    #   that seminar, but not those bound to workgroups, even without explicit permission
    # - if a user has view permission on a workgroup, they can see all items bound to
    #   that workgroup
    # - if a user has change permission on a seminar, they can change, delete and create
    #   items bound to that seminar
    # - if a user has change permission on a workgroup, they can change, delete and create
    #   items bound to that workgroup
    # - unbound items ("global items") are visible to everyone, but require superuser privileges
    #   or explicit permission to change, create or delete
    #
    # To make this into a useful user experience, permissions through guardian should be
    # configured as follows:
    # - server owners have superuser privileges, or some other group with explicit full privileges
    #   to the items in here
    # - seminar organizers receive view and change permission on their seminar as well as
    #   view/change/delete privileges on all workgroup (it is an error not to grant the latter)
    # - workgroup moderators receive view and change permission on their workgroup as well as view
    #   permission on their seminar (it is an error to not grant the latter)
    # - seminar organizers AND workgroup moderators receive global add permissions on events, event
    #   links, announcements, and meetings (the form logic makes sure they can't actually bind them to
    #   seminars or workgroups they don't have change permission for, nor create global items)
    # - seminar organizers also receive global add permissions on workgroups

    allow_global_items = True  # TODO rename this, we also have "allow_global_object"
    unrestricted_global_object_creation = False
    unrestricted_global_object_deletion = False
    no_add_on_workgroup = False

    def __init__(self, *args, **kwargs):
        super(SeminarBoundModelMixin, self).__init__(*args, **kwargs)
        # You can use the 'ref' attribute to set which model we should look at to get the permissions. This is
        # needed when you want to use InlineModelAdmin and the inlined object does not directly contain a reference to
        # a seminar or workgroup, but instead only one to the item that inlines it. In this case, set the reference to the
        # name of that reference field. (Of course, the inlining item must then have the workgroup(s) and seminar fields!)
        # You can also use it for the other way round, i.e. when you have your seminar bindings in an extension model.

        # checks if 'ref=mymodel' has been specified in the class where this mixin is used
        if hasattr(self, "ref"):
            self.filter_prefix = self.ref + "__"
            self.ref_model = self.model._meta.get_field(self.ref).related_model
            if isinstance(self, admin.options.InlineModelAdmin):
                self.permission_ref = None
                self.using_extension_model = False
            else:
                self.permission_ref = self.ref
                self.using_extension_model = True
        else:
            self.filter_prefix = ""
            self.ref_model = self.model
            if isinstance(self, admin.options.InlineModelAdmin):
                # this solves the quirks with inline admins described further down at the permission
                # methods
                self.using_extension_model = True
                self.permission_ref = self.model._meta.model_name
            else:
                self.permission_ref = None
                self.using_extension_model = False
        # TODO maybe change from hasattr to looking into the _meta?
        self.workgroups_field_exists = hasattr(self.ref_model, "workgroups")
        self.workgroup_field_exists = hasattr(self.ref_model, "workgroup")
        self.seminar_field_exists = hasattr(self.ref_model, "seminar")
        assert (
            self.workgroups_field_exists
            or self.workgroup_field_exists
            or self.seminar_field_exists
        ), "This model is not of seminar bound type"

    # makes sure that non-superusers can't create global objects, nor unbind objects so that
    # they become global

    def get_form(self, request, obj=None, **kwargs):
        # if ref is set, then we're either an inline or using an one-to-one extension model
        # to manage our seminar/workgropu binding, so it won't be handled from this form, so
        # no need to impose the restriction
        if hasattr(self, "ref"):
            return super(SeminarBoundModelMixin, self).get_form(request, obj, **kwargs)

        # forces this base class for the model form factory
        if self.form == ModelForm:
            kwargs["form"] = SeminarBoundProtectedModelForm
        else:
            assert issubclass(self.form, SeminarBoundProtectedModelForm), (
                "Your custom form needs to inherit from SeminarBoundProtectedModelForm or security will be broken",
                self.form,
            )
        f = super(SeminarBoundModelMixin, self).get_form(request, obj, **kwargs)
        f.allow_global_object = (
            request.user.is_superuser or self.unrestricted_global_object_creation
        )
        return f

    # TODO This is quite ugly, but it's flexible. Can we improve this?
    def get_implicit_queryset(self, request):
        query = self.make_implicit_query_filter(
            request,
            use_seminar=self.seminar_field_exists,
            use_workgroup=self.workgroup_field_exists,
            use_workgroups=self.workgroups_field_exists,
            filter_prefix=self.filter_prefix,
            relax=True,
        )
        return self.model.objects.filter(query)

    def get_queryset(self, request):
        qs = super(SeminarBoundModelMixin, self).get_queryset(request)
        iqs = self.get_implicit_queryset(request)
        # return (qs | iqs).distinct()

        # problem with the line above ^: After distinct() you can't delete(),
        # and after union(all=False), which would avoid distinct(), you can't filter().
        # The only easy fix is to write this using a subquery, first grabbing all the IDs
        # in the subquery, such that the main query doesn't get any of the aforementioned
        # restrictions.

        # Work around the fact that if an ordering is set on the Model or ModelAdmin,
        # then we'll get an ORDER BY in the SQL of qs, and iqs, but below we want to do UNION.
        # This is not supported by SQLite, so we clear the ordering here and set it
        # again after the UNION.
        orig_ordering = super(SeminarBoundModelMixin, self).get_ordering(request)
        # TODO For Django 4 the parameters names have changed here!
        qs.query.clear_ordering(force_empty=True)
        iqs.query.clear_ordering(force_empty=True)

        fix_query_chaining_hack = qs.only("id").union(iqs.only("id"))
        consolidated = self.model.objects.filter(id__in=fix_query_chaining_hack)

        if orig_ordering:
            consolidated = consolidated.order_by(*orig_ordering)
        return consolidated

    # (WARNING: Note that if we're an inline, our has_xy_permission methods will be called with
    # obj = the model instance of the thing that inlines us, not the object inside the inline)

    def has_view_permission(self, request, obj=None):
        if super(SeminarBoundModelMixin, self).has_view_permission(request, obj):
            return True
        if obj is None:
            # For inlines, we don't care if we "accidentally" return True when the list is emtpy
            # and could have been hidden, since hiding the list is controlled by the parent anyway.
            # (And running the code below would cause endless recursion due to the weird way that
            # Django handles inlines ...)
            if isinstance(self, admin.options.InlineModelAdmin):
                return True
            # We will always grant permission if the list is not empty AND the user doesn't have
            # permission to see anything (otherwise the list won't display).
            # It's get_queryset that controls what the user actually gets to see (might be nothing)
            return self.get_queryset(request).exists() or self.has_add_permission(
                request
            )
        if self.using_extension_model:
            obj = getattr(obj, self.permission_ref)
        if self.seminar_field_exists and obj.seminar is not None:
            return (
                self.get_viewable_seminars(request).filter(pk=obj.seminar.pk).exists()
            )
        if self.workgroups_field_exists:
            if obj.workgroups.exists():
                return (
                    self.get_viewable_workgroups(request)
                    .filter(pk__in=obj.workgroups.all())
                    .exists()
                )
        elif self.workgroup_field_exists:
            if obj.workgroup is not None:
                return (
                    self.get_viewable_workgroups(request)
                    .filter(pk=obj.workgroup.pk)
                    .exists()
                )
        return self.allow_global_items  # in this case always allow viewing global items

    # KNOWN BUG: When a user unassigns all workgroups and the seminar from an object and thus tries
    # to create a global object, this will fail as expected if they are not a superuser. However, the
    # object is somehow still modified by Django (but not saved to DB) even though the form validation
    # failed. This then causes the user to lose change permission on the temporary object, so they can't
    # undo their mistake without reloading the page.

    def has_change_permission(self, request, obj=None):
        if super(SeminarBoundModelMixin, self).has_change_permission(request, obj):
            return True
        if obj is not None:
            if self.using_extension_model:
                obj = getattr(obj, self.permission_ref)
            if self.seminar_field_exists and obj.seminar is not None:
                return (
                    self.get_changeable_seminars(request)
                    .filter(pk=obj.seminar.pk)
                    .exists()
                )
            if self.workgroups_field_exists:
                if obj.workgroups.exists():
                    return (
                        self.get_changeable_workgroups(request)
                        .filter(pk__in=obj.workgroups.all())
                        .exists()
                    )
            elif self.workgroup_field_exists:
                if obj.workgroup is not None:
                    return (
                        self.get_changeable_workgroups(request)
                        .filter(pk=obj.workgroup.pk)
                        .exists()
                    )
        # never allow changing global elements (parent class handles superuser perms)
        return False

    def has_delete_permission(self, request, obj=None):
        if super(SeminarBoundModelMixin, self).has_delete_permission(request, obj):
            return True
        # delete permission maps to change permission of seminar/workgroup
        if obj is not None:
            if self.using_extension_model:
                obj = getattr(obj, self.permission_ref)
            if self.seminar_field_exists and obj.seminar is not None:
                return (
                    self.get_changeable_seminars(request)
                    .filter(pk=obj.seminar.pk)
                    .exists()
                )
            if self.workgroups_field_exists:
                if obj.workgroups.exists():
                    return (
                        self.get_changeable_workgroups(request)
                        .filter(pk__in=obj.workgroups.all())
                        .exists()
                    )
            elif self.workgroup_field_exists:
                if obj.workgroup is not None:
                    return (
                        self.get_changeable_workgroups(request)
                        .filter(pk=obj.workgroup.pk)
                        .exists()
                    )
        # never allow deleting global elements (parent class handles superuser perms)
        return self.unrestricted_global_object_deletion

    def has_add_permission(self, request, obj=None):
        # Django is weird: InlineModelAdmin REQUIRES obj in has_add_permission,
        # ModelAdmin REJECTS it

        # check what we are used as a mixin on
        if isinstance(self, admin.options.InlineModelAdmin):
            if super(SeminarBoundModelMixin, self).has_add_permission(request, obj):
                return True
        else:
            if super(SeminarBoundModelMixin, self).has_add_permission(request):
                return True
        # user has add permissions if they have change permissions for any seminar or workgroup
        # (the dropdown fields set by the parent class will only allow referencing those seminars
        # or workgroups that the user has edit permissions on)
        return self.get_changeable_seminars(request).exists() or (
            not self.no_add_on_workgroup
            and self.get_changeable_workgroups(request).exists()
        )

    def has_module_permission(self, request):
        # if the user can see the list of items, based on our custom seminar-bound
        # permissions, then also grant module permission (otherwise the link to that
        # list would not be displayed)
        if self.has_view_permission(request, obj=None):
            return True
        # if the user has any global or per-object permissions on our model, then
        # also grant module permission (this check is more expensive, so we do it last)
        return super().has_module_permission(request)
