# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# adapted from https://gist.github.com/rca/7217540
# using https://www.openldap.org/faq/data/cache/347.html

import hashlib
import secrets

from base64 import encodebytes


def make_secret(password):
    """
    Encodes the given password as a base64 SSHA hash+salt buffer
    """
    salt = secrets.token_bytes(4)
    # hash the password and append the salt
    sha = hashlib.sha1(password.encode("utf-8"))
    sha.update(salt)
    # create a base64 encoded string of the concatenated digest + salt
    digest_salt_b64 = encodebytes(sha.digest() + salt).strip()
    # now tag the digest above with the {SSHA} tag
    tagged_digest_salt = "{SSHA}" + digest_salt_b64.decode("utf-8")
    return tagged_digest_salt
