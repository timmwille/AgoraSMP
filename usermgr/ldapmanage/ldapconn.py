# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.conf import settings

import usermgr.ldapmanage.ssha as ssha  # TODO clean up and unify import paths

import ldap
import ldap.modlist

import logging

logger = logging.getLogger(__name__)
logger_auth = logging.getLogger("agorasmp.auth")


class LDAPConnection:
    def __init__(self, host, binduser, passwd):
        self.host = host
        self.binduser = binduser
        self.passwd = passwd
        self.conn = None

    def open(self):
        conn = ldap.initialize(self.host, bytes_mode=False)
        conn.bind_s(self.binduser, self.passwd)
        self.conn = conn

    def search(self, base, filter="(objectClass=*)", attrlist=None):
        return self.conn.search_s(base, ldap.SCOPE_SUBTREE, filter, attrlist)

    def finduser(self, uid, attrlist=None):
        base = "ou=accounts," + settings.AUTH_LDAP_DC
        user_filter = "(uid={})".format(uid)
        results = self.search(base, user_filter, attrlist)
        if results:
            return results[0][1]
        return None

    def group_dn(self, cn):
        return "cn={},ou=groups,{}".format(cn, settings.AUTH_LDAP_DC)

    def user_dn(self, uid):
        return "uid={},ou=accounts,{}".format(uid, settings.AUTH_LDAP_DC)

    def user_ingroup(self, uid, gid):
        result = self.finduser(uid, ["memberOf"])
        if result is None:
            return None
        membership = [r.decode("utf-8") for r in result["memberOf"]]
        group = self.group_dn(gid)
        return group in membership

    def encode(self, entryset):
        encodedset = dict()
        for k, v in entryset.items():
            if v is None:
                enc_val = None
            elif type(v) == list:
                enc_val = [(bytes(e, "utf-8") if e is not None else None) for e in v]
            else:
                enc_val = bytes(v, "utf-8")
            encodedset[k] = enc_val
        print(encodedset)
        return encodedset

    def to_add_modlist(self, entryset):
        return ldap.modlist.addModlist(self.encode(entryset))

    def to_modify_modlist(self, oldEntryset, newEntryset):
        return ldap.modlist.modifyModlist(
            self.encode(oldEntryset), self.encode(newEntryset)
        )

    def group_create(self, gid, members):
        entries = {
            "objectClass": "groupOfNames",
            "cn": gid,
            "member": [self.user_dn(m) for m in members],
        }
        modlist = self.to_add_modlist(entries)
        dn = self.group_dn(gid)
        self.conn.add_s(dn, modlist)
        logger_auth.info('Initialized LDAP group "%s"', dn)

    def group_join(self, gid, member, gid_is_dn=False, uid_is_dn=False):
        # the python-ldap modlist creator is less than useful here,
        # so we make the modlist manually
        if not uid_is_dn:
            member = self.user_dn(member)
        modlist = [(ldap.MOD_ADD, "member", [bytes(member, "utf-8")])]
        if gid_is_dn:
            dn = gid
        else:
            dn = self.group_dn(gid)
        self.conn.modify_s(dn, modlist)
        logger_auth.info('User "%s" joined LDAP group "%s"', member, dn)

    def group_kick(self, gid, member, gid_is_dn=False, uid_is_dn=False):
        # the python-ldap modlist creator is less than useful here,
        # so we make the modlist manually
        if not uid_is_dn:
            member = self.user_dn(member)
        modlist = [(ldap.MOD_DELETE, "member", [bytes(member, "utf-8")])]
        if gid_is_dn:
            dn = gid
        else:
            dn = self.group_dn(gid)
        self.conn.modify_s(dn, modlist)
        logger_auth.info('User "%s" removed from LDAP group "%s"', member, dn)

    def user_create(self, uid, givenName, surName, mail, passwd):
        cryptpasswd = ssha.make_secret(passwd)
        entries = {
            "objectClass": ["top", "person", "organizationalPerson", "inetOrgPerson"],
            "uid": uid,
            "cn": uid,
            "givenName": givenName,
            "sn": surName,
            "displayName": givenName + " " + surName,
            "mail": mail,
            "userPassword": cryptpasswd,
        }
        modlist = self.to_add_modlist(entries)
        dn = self.user_dn(uid)
        self.conn.add_s(dn, modlist)
        logger_auth.info('Initialized LDAP user "%s"', dn)

    # call this ONCE to set an unique ID for each user, then never change it
    # (this is separate from user_create since the current implementation adds the
    # user to LDAP, then pulls it into Django, which makes the DB auto-assign an
    # unique Django User ID, which we then put back into LDAP by calling this
    # method)
    def user_set_id(self, uid, numid):
        modlist = [(ldap.MOD_ADD, "roomNumber", [bytes(str(numid), "utf-8")])]
        dn = self.user_dn(uid)
        self.conn.modify_s(dn, modlist)
        logger_auth.info('Set LDAP user ID for "%s" to %d', dn, numid)

    def user_replace_email(self, uid, email):
        modlist = [(ldap.MOD_REPLACE, "mail", [bytes(str(email), "utf-8")])]
        dn = self.user_dn(uid)
        self.conn.modify_s(dn, modlist)
        logger_auth.info('Set LDAP user email for "%s" to "%s"', dn, email)

    def user_replace_name(self, uid, givenName, surName):
        # TODO create modlist automatically
        modlist = [
            (ldap.MOD_REPLACE, "givenName", [bytes(str(givenName), "utf-8")]),
            (ldap.MOD_REPLACE, "sn", [bytes(str(surName), "utf-8")]),
            (
                ldap.MOD_REPLACE,
                "displayName",
                [bytes(str(givenName + " " + surName), "utf-8")],
            ),
        ]
        dn = self.user_dn(uid)
        self.conn.modify_s(dn, modlist)
        logger_auth.info(
            'Set LDAP user display name for "%s" to "%s %s"', dn, givenName, surName
        )

    def user_passwd(self, uid, passwd):
        # bound user must have write privilege to the userPassword attribute
        cryptpasswd = ssha.make_secret(passwd)
        modlist = [(ldap.MOD_REPLACE, "userPassword", [bytes(cryptpasswd, "utf-8")])]
        dn = self.user_dn(uid)
        self.conn.modify_s(dn, modlist)
        logger_auth.info('Replaced LDAP user password for "%s"', dn)

    def delete(self, dn):
        # no exception if dn does not exist
        self.conn.delete(dn)

    def user_delete(self, uid):
        dn = self.user_dn(uid)
        self.conn.delete(dn)
        logger_auth.info('Deleted LDAP user "%s"', dn)

    def group_delete(self, gid):
        dn = self.group_dn(gid)
        self.conn.delete(dn)
        logger_auth.info('Deleted LDAP group "%s"', dn)
