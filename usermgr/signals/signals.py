# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.dispatch import Signal

seminar_created = Signal(providing_args=["seminar"])
seminar_changed = Signal(providing_args=["seminar"])
seminar_access_opens = Signal(providing_args=["seminar"])
seminar_begins = Signal(providing_args=["seminar"])
seminar_ends = Signal(providing_args=["seminar"])
seminar_access_closes = Signal(providing_args=["seminar"])
seminar_destroyed = Signal(providing_args=["seminar"])

workgroup_created = Signal(providing_args=["workgroup"])
workgroup_changed = Signal(providing_args=["workgroup"])
workgroup_removed = Signal(providing_args=["workgroup"])

user_registered = Signal(providing_args=["profile"])
user_staff_appointed = Signal(providing_args=["user"])
user_staff_demoted = Signal(providing_args=["user"])
user_name_changed = Signal(providing_args=["user"])
user_email_changed = Signal(providing_args=["user"])
user_gdpr_delete = Signal(providing_args=["username", "userid"])

# These signals can be "synthetic", meaning they are fired because of some other
# signal. For example, "user_workgroup_assigned" fires for each user in each workgroup
# of a seminar that just opened (i.e. "seminar_access_opens" just fired).
# However, they also fire when some user joins or leaves a seminar or workgroup while
# the seminar is currently open. In that case, they are _not_ synthetic.

# Many times you don't care about the _cause_, you just care that the user should now
# have access to that seminar. In that case, you don't need to handle e.g.
# "seminar_access_opens" but only "user_workgroup_assigned" and "user_seminar_assigned".
# If you, however, need to do something special in on of the cases (like the Matrix
# worker, which handles the seminar_access_opens case specially so it can bulk-invite
# users and be a lot more efficient), then you just ignore all of the "xx_yy_assigned"
# signals that have the "synthetic" flag set, and implement the "seminar_access_opens"
# handler.

workgroup_access_opens = Signal(providing_args=["workgroup", "synthetic"])
workgroup_access_closes = Signal(providing_args=["workgroup", "synthetic"])

user_workgroup_assigned = Signal(providing_args=["profile", "workgroup", "synthetic"])
user_seminar_assigned = Signal(providing_args=["profile", "seminar", "synthetic"])
user_workgroup_unassigned = Signal(providing_args=["profile", "workgroup", "synthetic"])
user_seminar_unassigned = Signal(providing_args=["profile", "seminar", "synthetic"])

workgroup_moderator_assigned = Signal(providing_args=["profile", "workgroup"])
workgroup_moderator_unassigned = Signal(providing_args=["profile", "workgroup"])
seminar_admin_assigned = Signal(providing_args=["profile", "seminar"])
seminar_admin_unassigned = Signal(providing_args=["profile", "seminar"])

event_upcoming = Signal(providing_args=["event"])
