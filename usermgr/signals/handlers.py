# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging

from django.conf import settings
from django.contrib.auth.models import Group, User
from django.db import models
from django.dispatch import receiver
from guardian.shortcuts import assign_perm
from usermgr.ldapmanage.ldapconn import LDAPConnection
from usermgr.models import Profile, Seminar, Workgroup
from .signals import *

logger = logging.getLogger(__name__)
logger_auth = logging.getLogger("agorasmp.auth")


def send_logged(signal, **kwargs):
    result = signal.send_robust(**kwargs)
    for _, response in result:
        if response is not None:
            logger.error(response)


##### helper handlers


@receiver(models.signals.post_save, sender=Seminar, dispatch_uid="usermgr")
def on_seminar_saved(sender, **kwargs):
    seminar = kwargs["instance"]
    created = kwargs["created"]
    # fire the seminar_created signal upon insertion into the Seminar DB table
    if created:
        send_logged(seminar_created, sender=sender, seminar=seminar)
    else:
        send_logged(seminar_changed, sender=sender, seminar=seminar)


@receiver(
    models.signals.m2m_changed, sender=Seminar.admins.through, dispatch_uid="usermgr"
)
def on_seminar_admins_changed(sender, **kwargs):
    seminar = kwargs["instance"]
    action = kwargs["action"]
    # reverse = kwargs['reverse']
    user_ids = kwargs[
        "pk_set"
    ]  # seminar admins are users, not their profiles (maybe we should change that)
    logger.debug("on_seminar_admins_changed %s", action)
    if action == "post_add":
        for profile in Profile.objects.filter(user__in=user_ids):
            logger_auth.warning(
                'User %s assigned as admin for seminar "%s"',
                profile.user.username,
                seminar.slug,
            )
            send_logged(
                seminar_admin_assigned, sender=sender, profile=profile, seminar=seminar
            )
    elif action == "pre_remove":
        for profile in Profile.objects.filter(user__in=user_ids):
            logger_auth.warning(
                'User %s removed from admins of seminar "%s"',
                profile.user.username,
                seminar.slug,
            )
            send_logged(
                seminar_admin_unassigned,
                sender=sender,
                profile=profile,
                seminar=seminar,
            )
    elif action == "pre_clear":
        logger_auth.warning('Admins cleared for seminar "%s"', seminar.slug)
        for profile in Profile.objects.filter(user__in=seminar.admins):
            send_logged(
                seminar_admin_unassigned,
                sender=sender,
                profile=profile,
                seminar=seminar,
            )


# pre_save because we need to compute the delta
@receiver(models.signals.pre_save, sender=User, dispatch_uid="usermgr")
def on_user_saved_pre(sender, **kwargs):
    user = kwargs["instance"]
    if "last_login" in (kwargs.get("update_fields") or set()):
        logger_auth.info("User %s signed in", user.username)

    # FIXME There is a slight issue here: When a user logs in and someone has manually changed their
    # LDAP entry, then those events will also fire, which will then in turn "update" the LDAP entry
    # with the information that was just read from it. This is not breaking anything, it's just an
    # unnecessary LDAP transaction and should be avoided.

    # With the way that our Celery integration works, we can't fire signals that may trigger Celery
    # tasks from within a pre_save signal, since this would cause a database race: If the Celery task
    # runs before the model instance is saved (and a potential transaction committed), then it will
    # see the old user object *without* the changes.
    # This means that:
    #  - we can only trigger Celery tasks from post_save
    #  - but we can only see which fields have changed in pre_save
    # ... which leads to this workaround:
    user.__postsave_callbacks = []

    try:
        old_user = User.objects.get(pk=user.pk)
    except User.DoesNotExist:
        return  # this happens on initial user creation, which we don't want to handle at all
        # (We _do_ handle it, but we do it when the attached _Profile_ is created)

    # we log those two in the view form, no logger here
    if user.email != old_user.email:
        user.__postsave_callbacks.append(
            lambda: send_logged(user_email_changed, sender=sender, user=user)
        )
    if user.first_name != old_user.first_name or user.last_name != old_user.last_name:
        user.__postsave_callbacks.append(
            lambda: send_logged(user_name_changed, sender=sender, user=user)
        )

    if user.is_staff != old_user.is_staff:
        if user.is_staff:
            logger_auth.warning("User %s promoted to staff", user.username)
            user.__postsave_callbacks.append(
                lambda: send_logged(user_staff_appointed, sender=sender, user=user)
            )
        else:
            logger_auth.warning("User %s demoted from staff", user.username)
            user.__postsave_callbacks.append(
                lambda: send_logged(user_staff_demoted, sender=sender, user=user)
            )


@receiver(models.signals.post_save, sender=User, dispatch_uid="usermgr")
def on_user_saved_post(sender, **kwargs):
    user = kwargs["instance"]
    assert hasattr(
        user, "__postsave_callbacks"
    ), "__postsave_callbacks missing on user instance; did the pre_save hook run correctly?"
    for cb in user.__postsave_callbacks:
        cb()


@receiver(models.signals.post_save, sender=Workgroup, dispatch_uid="usermgr")
def on_workgroup_saved(sender, **kwargs):
    workgroup = kwargs["instance"]
    created = kwargs["created"]
    # fire the workgroup_created signal upon insertion into the Workgroup DB table
    if created:
        logger.info('Workgroup "%s" created', workgroup.slug)
        workgroup_created.send_robust(sender=sender, workgroup=workgroup)
        if workgroup.seminar.is_access_open():
            send_logged(
                workgroup_access_opens,
                sender=sender,
                workgroup=workgroup,
                synthetic=False,
            )
        else:
            logger.info("access not open, no action taken")
    else:
        logger.info('Workgroup "%s" modified', workgroup.slug)
        send_logged(workgroup_changed, sender=sender, workgroup=workgroup)


@receiver(
    models.signals.m2m_changed,
    sender=Workgroup.moderators.through,
    dispatch_uid="usermgr",
)
def on_workgroup_moderators_changed(sender, **kwargs):
    workgroup = kwargs["instance"]
    action = kwargs["action"]
    # reverse = kwargs['reverse']
    user_ids = kwargs["pk_set"]
    logger.debug("on_workgroup_moderators_changed {}".format(action))
    if action == "post_add":
        for profile in Profile.objects.filter(user__in=user_ids):
            logger_auth.warning(
                'User %s assigned as moderator for workgroup "%s"',
                profile.user.username,
                workgroup.slug,
            )
            send_logged(
                workgroup_moderator_assigned,
                sender=sender,
                profile=profile,
                workgroup=workgroup,
            )
    elif action == "pre_remove":
        for profile in Profile.objects.filter(user__in=user_ids):
            logger_auth.warning(
                'User %s removed from moderators of workgroup "%s"',
                profile.user.username,
                workgroup.slug,
            )
            send_logged(
                workgroup_moderator_unassigned,
                sender=sender,
                profile=profile,
                workgroup=workgroup,
            )
    elif action == "pre_clear":
        logger_auth.warning('Moderators cleared for workgroup "%s"', workgroup.slug)
        for profile in Profile.objects.filter(user__in=workgroup.moderators):
            send_logged(
                workgroup_moderator_unassigned,
                sender=sender,
                profile=profile,
                workgroup=workgroup,
            )


@receiver(models.signals.post_save, sender=Profile, dispatch_uid="usermgr")
def on_profile_saved(sender, **kwargs):
    profile = kwargs["instance"]
    created = kwargs["created"]
    # fire the user_registered signal upon insertion into the Profile DB table
    if created:
        logger_auth.debug("Profile initialized for user %s", profile.user.username)
        send_logged(user_registered, sender=sender, profile=profile)


@receiver(
    models.signals.m2m_changed,
    sender=Profile.workgroups.through,
    dispatch_uid="usermgr",
)
def on_profile_workgroup_saved(sender, **kwargs):
    reverse = kwargs["reverse"]
    action = kwargs["action"]

    # HACK synthesize the non-reversed event and re-fire
    if reverse:
        if "clear" in action:
            # we're firing single events for this
            action = action.replace("clear", "remove")
        profile_ids = kwargs["pk_set"]
        wg_id = kwargs["instance"].pk
        for pid in profile_ids:
            profile = Profile.objects.get(pk=pid)
            on_profile_workgroup_saved(
                sender, instance=profile, pk_set=[wg_id], action=action, reverse=False
            )
        return

    profile = kwargs["instance"]
    wg_ids = kwargs["pk_set"]
    logger.debug("on_profile_workshop_changed {}".format(action))
    if action == "post_add":
        seminars_to_assign = set()
        for wg_id in wg_ids:
            # assign workgroup if the seminar is open
            # (otherwise this would allow a user to access not yet open or already closed seminars)
            workgroup = Workgroup.objects.get(pk=wg_id)  # TODO check performance
            if workgroup.seminar.is_access_open():
                logger.info(
                    'User %s added to members of workgroup "%s"',
                    profile.user.username,
                    workgroup.slug,
                )
                send_logged(
                    user_workgroup_assigned,
                    sender=sender,
                    profile=profile,
                    workgroup=workgroup,
                    synthetic=False,
                )
                # if this user is not already in other workgroups of this seminar, also assign the seminar
                existing_workgroups = (
                    profile.workgroups.exclude(pk__in=wg_ids)
                    .filter(seminar=workgroup.seminar)
                    .exists()
                )
                if not existing_workgroups:
                    seminars_to_assign.add(workgroup.seminar)
        for seminar in seminars_to_assign:
            logger.info(
                'User %s added to members of seminar "%s"',
                profile.user.username,
                seminar.slug,
            )
            send_logged(
                user_seminar_assigned,
                sender=sender,
                profile=profile,
                seminar=seminar,
                synthetic=False,
            )
    elif action == "pre_remove":
        seminars_to_unassign = set()
        for wg_id in wg_ids:
            # unassign workgroup if seminar is still open
            # (otherwise the user is already unassigned)
            workgroup = Workgroup.objects.get(pk=wg_id)  # TODO check performance
            if workgroup.seminar.is_access_open():
                logger.info(
                    'User %s removed from members of workgroup "%s"',
                    profile.user.username,
                    workgroup.slug,
                )
                send_logged(
                    user_workgroup_unassigned,
                    sender=sender,
                    profile=profile,
                    workgroup=workgroup,
                    synthetic=False,
                )
            # check if the user has another workgroup in this seminar
            remaining_workgroups = (
                profile.workgroups.exclude(pk__in=wg_ids)
                .filter(seminar=workgroup.seminar)
                .exists()
            )
            if not remaining_workgroups:
                seminars_to_unassign.add(workgroup.seminar)
        for seminar in seminars_to_unassign:
            logger.info(
                'User %s removed from members of seminar "%s"',
                profile.user.username,
                seminar.slug,
            )
            send_logged(
                user_seminar_unassigned,
                sender=sender,
                profile=profile,
                seminar=seminar,
                synthetic=False,
            )
    elif action == "pre_clear":
        logger.info("Memberships cleared for user %s", profile.user.username)
        # unassign all workgroups
        seminars = set()
        for workgroup in profile.workgroups.all():
            if workgroup.seminar.is_access_open():
                send_logged(
                    user_workgroup_unassigned,
                    sender=sender,
                    profile=profile,
                    workgroup=workgroup,
                )
                seminars.add(
                    workgroup.seminar
                )  # keep track of the seminar this workshop belongs to
        # unassign all seminars
        for seminar in seminars:
            send_logged(
                user_seminar_unassigned, sender=sender, profile=profile, seminar=seminar
            )


@receiver(seminar_access_opens, dispatch_uid="usermgr")
def on_seminar_access_opens(sender, **kwargs):
    # assign all users to the necessary groups as soon as the seminar opens
    # (all users that register later or that an admin later moves to another
    # workgroup will be joined by on_profile_workshop_saved())
    seminar = kwargs["seminar"]
    logger.debug("on_seminar_access_opens %s", seminar.name)
    workgroups = seminar.workgroup_set.all()
    all_profiles = set()
    for wg in workgroups:
        send_logged(workgroup_access_opens, sender=sender, workgroup=wg, synthetic=True)
        profiles = wg.profile_set.all()
        all_profiles.update(profiles)
        for profile in profiles:
            send_logged(
                user_workgroup_assigned,
                sender=sender,
                profile=profile,
                workgroup=wg,
                synthetic=True,
            )
    for profile in all_profiles:
        send_logged(
            user_seminar_assigned,
            sender=sender,
            profile=profile,
            seminar=seminar,
            synthetic=True,
        )


@receiver(seminar_access_closes, dispatch_uid="usermgr")
def on_seminar_access_closes(sender, **kwargs):
    # remove all users from the necessary groups as soon as the seminar closes
    # (note that they will keep the workgroups in their profile, they're just
    # removed from the LDAP groups so that the satellite services know not to
    # display content from that seminar anymore)
    seminar = kwargs["seminar"]
    logger.debug("on_seminar_access_closes {}".format(seminar.name))
    workgroups = seminar.workgroup_set.all()
    all_profiles = set()
    for wg in workgroups:
        send_logged(
            workgroup_access_closes, sender=sender, workgroup=wg, synthetic=True
        )
        profiles = wg.profile_set.all()
        all_profiles.update(profiles)
        for profile in profiles:
            send_logged(
                user_workgroup_unassigned,
                sender=sender,
                profile=profile,
                workgroup=wg,
                synthetic=True,
            )
    for profile in all_profiles:
        send_logged(
            user_seminar_unassigned,
            sender=sender,
            profile=profile,
            seminar=seminar,
            synthetic=True,
        )


##### LDAP management handlers

# TODO maybe move this to a better place
def make_group(group):
    logger_auth.info('LDAP group "%s" initialized', group)
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    ad.group_create(group, ["superuser"])


@receiver(seminar_created, dispatch_uid="usermgr")
def on_seminar_created(sender, **kwargs):
    seminar = kwargs["seminar"]
    logger.info('Setting up groups and permissions for seminar "%s"', seminar.slug)
    # TODO we _should_ never se a collision here, unless someone messes with the LDAP manually
    # maybe check anyway?
    make_group("s-" + seminar.slug)
    make_group("admin-" + seminar.slug)
    # make our Django-internal group whose members are then synced from LDAP
    group = Group.objects.create(name="admin-" + seminar.slug)
    assign_perm("view_seminar", group, seminar)
    assign_perm("change_seminar", group, seminar)
    assign_perm("usermgr.add_workgroup", group)  # globally
    # group.permissions.add(Permission.objects.get(codename='add_workgroup'), group) # globally
    logger.debug('Group and permission setup complete for seminar "%s"', seminar.slug)


@receiver(workgroup_created, dispatch_uid="usermgr")
def on_workgroup_created(sender, **kwargs):
    workgroup = kwargs["workgroup"]
    logger.info('Setting up groups and permissions for workgroup "%s"', workgroup.slug)
    # TODO we _should_ never se a collision here, unless someone messes with the LDAP manually
    # maybe check anyway?
    make_group("w-" + workgroup.slug)
    make_group("mod-" + workgroup.slug)
    group = Group.objects.create(name="mod-" + workgroup.slug)
    assign_perm("view_seminar", group, workgroup.seminar)
    assign_perm("view_workgroup", group, workgroup)
    assign_perm("change_workgroup", group, workgroup)
    logger.debug(
        'Group and permission setup complete for workgroup "%s"', workgroup.slug
    )


@receiver(user_workgroup_assigned, dispatch_uid="usermgr")
def on_user_workgroup_assigned(sender, **kwargs):
    logger.debug("on_user_workgroup_assigned")
    profile = kwargs["profile"]
    workgroup = kwargs["workgroup"]
    username = profile.user.username
    seminar = workgroup.seminar
    group = "w-" + workgroup.slug
    seminar_group = "s-" + seminar.slug
    # TODO maybe we could have a central connection object somewhere
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    # join the workshop group
    # (membership test is just a state divergence safeguard)
    if not ad.user_ingroup(username, group):
        ad.group_join(group, username)


@receiver(user_seminar_assigned, dispatch_uid="usermgr")
def on_user_seminar_assigned(sender, **kwargs):
    logger.debug("on_user_seminar_assigned")
    profile = kwargs["profile"]
    username = profile.user.username
    seminar = kwargs["seminar"]
    seminar_group = "s-" + seminar.slug
    # TODO maybe we could have a central connection object somewhere
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    # if the user is not already a member of the
    # seminar, join the seminar group
    if not ad.user_ingroup(username, seminar_group):
        ad.group_join(seminar_group, username)


@receiver(user_workgroup_unassigned, dispatch_uid="usermgr")
def on_user_workgroup_unassigned(sender, **kwargs):
    logger.debug("on_user_workgroup_unassigned")
    profile = kwargs["profile"]
    workgroup = kwargs["workgroup"]
    username = profile.user.username
    group = "w-" + workgroup.slug
    logger.info("{} {}".format(username, group))
    # TODO maybe we could have a central connection object somewhere
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    if ad.user_ingroup(username, group):
        ad.group_kick(group, username)


@receiver(user_seminar_unassigned, dispatch_uid="usermgr")
def on_user_seminar_unassigned(sender, **kwargs):
    logger.debug("on_user_seminar_unassigned")
    profile = kwargs["profile"]
    username = profile.user.username
    seminar = kwargs["seminar"]
    seminar_group = "s-" + seminar.slug
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    if ad.user_ingroup(username, seminar_group):
        ad.group_kick(seminar_group, username)


@receiver(user_staff_appointed, dispatch_uid="usermgr")
def on_user_staff_appointed(sender, **kwargs):
    logger.debug("on_user_staff_appointed")
    user = kwargs["user"]
    # update user in LDAP
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    ad.group_join(
        settings.AUTH_LDAP_USER_FLAGS_BY_GROUP["is_staff"],
        user.username,
        gid_is_dn=True,
    )
    logger.info("Staff group membership change in LDAP complete")


@receiver(user_staff_demoted, dispatch_uid="usermgr")
def on_user_staff_demoted(sender, **kwargs):
    logger.debug("on_user_staff_demoted")
    user = kwargs["user"]
    # update user in LDAP
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    ad.group_kick(
        settings.AUTH_LDAP_USER_FLAGS_BY_GROUP["is_staff"],
        user.username,
        gid_is_dn=True,
    )
    logger.info("Staff group membership change in LDAP complete")


@receiver(user_name_changed, dispatch_uid="usermgr")
def on_user_name_changed(sender, **kwargs):
    logger.debug("on_user_name_changed")
    user = kwargs["user"]
    # update user in LDAP
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    ad.user_replace_name(user.username, user.first_name, user.last_name)
    logger.info("Name change in LDAP complete")


@receiver(user_email_changed, dispatch_uid="usermgr")
def on_user_email_changed(sender, **kwargs):
    logger.debug("on_user_email_changed")
    user = kwargs["user"]
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    ad.user_replace_email(user.username, user.email)
    logger.info("Email change in LDAP complete")


@receiver(workgroup_moderator_assigned, dispatch_uid="usermgr")
def on_workgroup_moderator_assigned(sender, **kwargs):
    workgroup = kwargs["workgroup"]
    profile = kwargs["profile"]
    modgroup_name = "mod-" + workgroup.slug
    username = profile.user.username
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    if not ad.user_ingroup(username, modgroup_name):
        ad.group_join(modgroup_name, username)
        # also force-join immediately to the Django group, until we find out why the LDAP plugin doesn't auto-detect this
        internal_group = Group.objects.get(name=modgroup_name)
        profile.user.groups.add(internal_group)


@receiver(workgroup_moderator_unassigned, dispatch_uid="usermgr")
def on_workgroup_moderator_unassigned(sender, **kwargs):
    workgroup = kwargs["workgroup"]
    profile = kwargs["profile"]
    modgroup_name = "mod-" + workgroup.slug
    username = profile.user.username
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    if ad.user_ingroup(username, modgroup_name):
        ad.group_kick(modgroup_name, username)
        # also force-leave immediately from the Django group, until we find out why the LDAP plugin doesn't auto-detect this
        internal_group = Group.objects.get(name=modgroup_name)
        profile.user.groups.remove(internal_group)


@receiver(seminar_admin_assigned, dispatch_uid="usermgr")
def on_seminar_admin_assigned(sender, **kwargs):
    seminar = kwargs["seminar"]
    profile = kwargs["profile"]
    admingroup_name = "admin-" + seminar.slug
    username = profile.user.username
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    if not ad.user_ingroup(username, admingroup_name):
        ad.group_join(admingroup_name, username)
        # also force-join immediately to the Django group, until we find out why the LDAP plugin doesn't auto-detect this
        internal_group = Group.objects.get(name=admingroup_name)
        profile.user.groups.add(internal_group)


@receiver(seminar_admin_unassigned, dispatch_uid="usermgr")
def on_seminar_admin_unassigned(sender, **kwargs):
    seminar = kwargs["seminar"]
    profile = kwargs["profile"]
    admingroup_name = "admin-" + seminar.slug
    username = profile.user.username
    ad = LDAPConnection(
        settings.AUTH_LDAP_SERVER_URI,
        settings.AUTH_LDAP_BIND_DN,
        settings.AUTH_LDAP_BIND_PASSWORD,
    )
    ad.open()
    if ad.user_ingroup(username, admingroup_name):
        ad.group_kick(admingroup_name, username)
        # also force-leave immediately from the Django group, until we find out why the LDAP plugin doesn't auto-detect this
        internal_group = Group.objects.get(name=admingroup_name)
        profile.user.groups.remove(internal_group)
