# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils import timezone
from django import forms
from guardian.shortcuts import get_objects_for_user
from guardian.admin import GuardedModelAdmin
from common.guardian_extensions import PerItemPermsModelMixin
from common.security import is_safe_url
import re
import json
import unicodedata
import string

from import_export import resources, fields, widgets
from import_export.admin import ImportExportModelAdmin

from .models import *
from .admin_base import (
    SeminarBoundModelMixin,
    UsingSeminarBoundFieldMixin,
    SeminarBoundProtectedModelForm,
)

#### SEMINAR ####


class SeminarFilter(admin.RelatedFieldListFilter):

    # ensures that only seminars that the user has permission to see end up in
    # the filter, so that the user can't use the filter dropdown to peek at stuff
    # that they're no allowed to see
    def field_choices(self, field, request, model_admin):
        choices = super().field_choices(field, request, model_admin)
        viewable = set(
            get_objects_for_user(
                request.user, ["view_seminar"], klass=Seminar
            ).values_list("id", flat=True)
        )
        return list(filter(lambda c: c[0] in viewable, choices))  # TODO slow


class SeminarForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        start = cleaned_data.get("start_date")
        end = cleaned_data.get("end_date")
        if start is None or end is None:
            assert self._errors
            return cleaned_data
        if start > end:
            raise forms.ValidationError("Start date must not be past end date.")
        return cleaned_data


@admin.register(Seminar)
class SeminarAdmin(PerItemPermsModelMixin, admin.ModelAdmin):
    form = SeminarForm
    filter_horizontal = ["admins", "applications"]
    list_display = [
        "name",
        "start_date",
        "end_date",
        "state",
        "registered_participants",
    ]
    relax_reference_permissions = [Application]

    def get_prepopulated_fields(self, request, obj=None):
        if not obj:
            return {"slug": ("name",)}
        return {}

    def get_readonly_fields(self, request, obj=None):
        ro = []
        if obj:
            ro.append("slug")
            # FIXME currently the workers fuck up if you rename running seminars,
            # since they use the name and not the slug. Once that is fixed, remove
            # the next two lines.
            if obj.id and obj.state != Seminar.States.CREATED:
                ro.append("name")
        return ro

    def get_changeform_initial_data(self, request):
        return {"state": Seminar.States.CREATED}

    def registered_participants(self, obj):
        participants = ExpectedParticipant.objects.filter(
            pk__in=obj.workgroup_set.values_list("expectedparticipant", flat=True)
        )
        total = participants.count()
        if total == 0:
            return "no participants set"
        reg = participants.filter(registered=True).count()
        return "{} of {} ({}%)".format(reg, total, 100 * reg // total)


#### WORKGROUP ####


class WorkgroupFilter(admin.RelatedFieldListFilter):

    # ensures that only workgroups that the user has permission to see end up in
    # the filter, so that the user can't use the filter dropdown to peek at stuff
    # that they're no allowed to see
    def field_choices(self, field, request, model_admin):
        choices = super().field_choices(field, request, model_admin)
        viewable = set(
            get_objects_for_user(
                request.user, ["view_workgroup"], klass=Workgroup
            ).values_list("id", flat=True)
        )
        return list(filter(lambda c: c[0] in viewable, choices))  # TODO slow


# adapted from https://gist.github.com/Grokzen/a64321dd69339c42a184
class WorkgroupForm(SeminarBoundProtectedModelForm):

    members = forms.ModelMultipleChoiceField(
        queryset=Profile.objects.none(),  # can't not specify this, __init__ will reset this
        required=False,
        widget=FilteredSelectMultiple(verbose_name="Members", is_stacked=False),
    )

    # override me when subclassing this form using modelform_factory
    # (Yes, this field is static, but modelform_factory always creates a
    # subclass on the fly instead of an instance. Yes that's weird, but
    # Django wants it that way.)
    user_queryset = Profile.objects.none()

    class Meta:
        model = Workgroup
        exclude = []

    def __init__(self, *args, **kwargs):
        super(WorkgroupForm, self).__init__(*args, **kwargs)

        self.fields["members"].queryset = self.user_queryset
        if self.instance and self.instance.pk:
            # Note: This will let you see users that you don't have view permission for if they are
            # members of your workgroup. It's a tradeoff between privacy of users and "why the f***
            # can't I remove this user from my workgroup?"
            self.fields["members"].initial = self.instance.profile_set.get_queryset()

    def save(self, commit=True):
        workgroup = super(WorkgroupForm, self).save(commit=False)
        if commit:
            workgroup.save()
        if workgroup.pk:
            workgroup.profile_set.set(self.cleaned_data["members"])
            self.save_m2m()
        return workgroup


@admin.register(Workgroup)
class WorkgroupAdmin(
    UsingSeminarBoundFieldMixin, SeminarBoundModelMixin, admin.ModelAdmin
):
    form = WorkgroupForm
    filter_horizontal = ["moderators"]
    list_display = ["name", "seminar", "registered_participants"]
    list_filter = [("seminar", SeminarFilter)]

    allow_global_items = False  # controls SeminarBoundModelMixin

    # otherwise you get add permission because you can edit some other workgroup
    # but what we want is you to have seminar edit permission
    no_add_on_workgroup = True

    def get_form(self, request, obj=None, change=False, **kwargs):
        klass = super().get_form(request, obj=obj, change=change, **kwargs)
        # This overrides the static field on the subclass dynamically generated by
        # modelform_factory() called through super().get_form(). Yes it's weird,
        # but that's how Django wants it.
        if request.user.has_perm("change_user"):
            klass.user_queryset = Profile.objects.all()
        else:
            klass.user_queryset = self.get_implicit_queryset_for_foreign(
                request, Profile
            ).distinct()
        return klass

    def get_prepopulated_fields(self, request, obj=None):
        if not obj:
            return {"slug": ("name",)}
        return {}

    def get_readonly_fields(self, request, obj=None):
        ro = []
        if obj:
            ro += [
                "seminar",  # prevent workgroup from being moved to another seminar
                "slug",  # slug is basically a pointer, renaming it would mess everything up
            ]
            # FIXME currently the workers fuck up if you rename workgroups on running seminars,
            # since they use the name and not the slug. Once that is fixed, remove the next two
            # lines.
            if obj.seminar.state != Seminar.States.CREATED:
                ro.append("name")
        return ro

    # WARNING: The way that user profiles currently work, a user can't be in a seminar without any workgroups.
    # In fact, seminar membership is implied through workgroup membership. The following filter makes sure that
    # the seminar organizers can't see any users that are not mapped to their seminar. This means that once the
    # user is removed from the last workgroup of any seminar that the organizer can see, the organizer can't
    # see this user anymore. Thus, they can't add the user to any other workgroup (only superusers can).

    # While this gives some privacy protection, it also forces seminar organizers to FIRST assign the new workgroup
    # and THEN remove the old assignment, if they want to move a user to a new workgroup. We should consider
    # changing that model and giving users explicit seminar membership.

    def registered_participants(self, obj):
        participants = obj.expectedparticipant_set
        total = participants.count()
        if total == 0:
            return "no participants set"
        reg = participants.filter(registered=True).count()
        return "{} of {} ({}%)".format(reg, total, 100 * reg // total)


#### PARTICIPANT LIST ####


class DefaultTimeField(fields.Field):
    def clean(self, data, **kwargs):
        d = super().clean(data, **kwargs)
        # if the field doesn't make any sense super().clean returns an empty string,
        # so we fill the field with the current time
        if d == "":
            return str(timezone.now())
        return d


class DefaultUsernameField(fields.Field):
    def clean_namepart(self, part):
        parts = part.strip().lower().split(" ")
        namepart = parts[0]
        i = 1
        while len(namepart) < 25 and i < len(parts):
            namepart += "-" + parts[i]
            i += 1
        namepart = namepart[:25]
        return namepart

    def clean(self, data, **kwargs):
        d = super().clean(data, **kwargs)
        # if the field doesn't make any sense super().clean returns an empty string,
        # so we generate the user name ourselves

        # TODO check if this is good Django practice to access other fields
        u = get_user_model()

        if d == "":
            # combine "given_name.surname" while making sure not to go over 51 chars
            part1 = self.clean_namepart(data["given_name"])
            part2 = self.clean_namepart(data["surname"])
            raw = "{}.{}".format(part1, part2)
            # splice unicode chars (accents, umlauts, etc.) into two-byte composite characters,
            # then keep only their ASCII base, then go from byte array back to utf-8 for
            # further processing
            normalized = (
                unicodedata.normalize("NFKD", raw)
                .encode("ASCII", "ignore")
                .decode("utf-8")
            )

            # make sure the username is unique, otherwise add a counter and try again
            # until we find an unique one
            if u.objects.filter(
                username=normalized
            ).exists() or DeletedUser.objects.account_existed(normalized):
                suffix = 1
                uname = normalized + str(suffix)
                while u.objects.filter(
                    username=uname
                ).exists() or DeletedUser.objects.account_existed(normalized):
                    suffix += 1
                    uname = normalized + str(suffix)
                    # failsafe against endless loop if someone ever introduces a bug here
                    if suffix > 100:
                        raise Exception(
                            "Potential bug: Large name clash detected - logic flawed?"
                        )
                d = uname
            else:
                d = normalized

        return d


class WorkgroupsField(fields.Field):
    def clean(self, data, **kwargs):
        # d = super().clean(data, **kwargs)
        d = data[self.column_name]
        # inflate from list entry to our seminar/workshop based data model
        array = json.loads(d)
        workgroups = Workgroup.objects.filter(slug__in=array)
        assert len(array) == workgroups.count()
        return workgroups

    def export(self, participant):
        try:
            # flatten the model into a single list
            workgroups = participant.workgroups.all()
            return json.dumps([w.slug for w in workgroups])
        except ValueError:
            return "Not available yet"


class ExpectedParticipantResource(resources.ModelResource):
    time_imported = DefaultTimeField(
        attribute="time_imported", column_name="time_imported", default=""
    )
    username = DefaultUsernameField(
        attribute="username", column_name="username", default=""
    )
    workgroups = WorkgroupsField(
        attribute="workgroups",
        column_name="workgroups",
        widget=widgets.ManyToManyWidget(Workgroup),
    )

    class Meta:
        model = ExpectedParticipant
        fields = (
            "id",
            "email",
            "given_name",
            "surname",
            "username",
            "time_imported",
            "workgroups",
        )
        skip_unchanged = True
        report_skipped = True

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        # if we don't see the "time_imported" column, synthesize it
        # (the DefaultTimeField's clean() method will take care of filling it)
        if "time_imported" not in dataset.headers:
            dataset.append_col([""] * len(dataset), header="time_imported")
        # if we don't see the "username" column, synthesize it
        if "username" not in dataset.headers:
            dataset.append_col([""] * len(dataset), header="username")
        # if we don't see the "id" column, synthesize it
        # (otherwise the DB engine will complain)
        # note that we don't fill the column (auto_increment does that for us), it
        # only needs to exist at all
        if "id" not in dataset.headers:
            dataset.append_col([""] * len(dataset), header="id")


class ExpectedParticipantForm(SeminarBoundProtectedModelForm):
    def clean_username(self):
        name = self.cleaned_data["username"]
        if not set(name).issubset(set(string.ascii_lowercase + string.digits + ".-")):
            raise ValidationError(
                "Invalid characters in username. Only lowercase letters, digits, dots and dashes are allowed."
            )
        return name

    def clean(self):
        cleaned_data = super().clean()
        registered = cleaned_data.get("registered")
        name = cleaned_data.get("username")

        if name is None:
            assert self._errors  # ensure that the missing field was detected by Django
            return cleaned_data

        if not registered:
            if get_user_model().objects.filter(username=name).exists():
                raise ValidationError(
                    "A user with this name already exists, but the 'registered' flag is not set.\nThis participant would not be able to register."
                )
            if DeletedUser.objects.account_existed(name):
                raise ValidationError(
                    "A user with this name once existed. You can't re-use old usernames."
                )
        else:
            if not get_user_model().objects.filter(username=name).exists():
                raise ValidationError(
                    "You can't set the 'registered' flag for a participant that is not actually registered"
                )
        return cleaned_data


@admin.register(ExpectedParticipant)
class ExpectedParticipantAdmin(
    UsingSeminarBoundFieldMixin, SeminarBoundModelMixin, ImportExportModelAdmin
):
    form = ExpectedParticipantForm
    resource_class = ExpectedParticipantResource
    list_display = [
        "given_name",
        "surname",
        "email",
        "username",
        "time_imported",
        "registered",
    ]
    search_fields = ["given_name", "surname", "email"]
    list_filter = [
        ("registered", admin.BooleanFieldListFilter),
        ("workgroups", WorkgroupFilter),
        ("workgroups__seminar", SeminarFilter),
    ]

    # everybody can create EPs that are not bound to anything, it's not dangerous
    # (Other models like Event imply not bound = part of every seminar. This one
    # does not, so it's fine)
    # [Just be careful - only super admins can delete them again!]
    unrestricted_global_object_creation = True


# admin.site.register(ExpectedParticipant, ExpectedParticipantAdmin)

#### USER ####
# (override default user admin)

# Define an inline admin descriptor for Profile model
# which acts a bit like a singleton
class ProfileInline(
    UsingSeminarBoundFieldMixin, SeminarBoundModelMixin, admin.StackedInline
):
    model = Profile
    can_delete = False
    verbose_name_plural = "profiles"
    filter_horizontal = ["workgroups"]


# Define a new User admin
class UserAdmin(SeminarBoundModelMixin, BaseUserAdmin):
    inlines = (ProfileInline,)
    list_filter = [
        ("profile__workgroups", WorkgroupFilter),
        ("is_staff", admin.BooleanFieldListFilter),
        ("is_superuser", admin.BooleanFieldListFilter),
        ("is_active", admin.BooleanFieldListFilter),
    ]
    ref = "profile"

    def get_exclude(self, request, obj=None):
        fields = []
        if not request.user.is_superuser:
            # FIXME
            pass  # fields += ["password", "user_permissions", "groups", "date_joined", "last_login"]
        return fields

    def get_readonly_fields(self, request, obj=None):
        fields = []
        if not request.user.is_superuser:
            fields += [
                "username",
                "is_staff",
                "is_superuser",
                "groups",
                "user_permissions",
                "date_joined",
                "last_login",
            ]
            # fields += ["username", "is_staff", "is_superuser"]
            if obj is not None:
                # non-superusers can only en-/disable users that are not staff themselves
                # (otherwise they could lock superusers or other staff out)
                if obj.is_staff or obj.is_superuser:
                    fields += ["is_active"]
        return fields

    # disable the mixin for add and delete permissions
    # (those are things that only the super users ever deal with)
    def has_add_permission(self, request):
        return super(BaseUserAdmin, self).has_add_permission(request)

    def has_delete_permission(self, request, obj=None):
        return super(BaseUserAdmin, self).has_delete_permission(request, obj)


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

#### EVENTS ####
class EventForm(SeminarBoundProtectedModelForm):
    def clean(self):
        cleaned_data = super().clean()
        start = cleaned_data.get("start_date")
        end = cleaned_data.get("end_date")
        if start is None or end is None:
            assert self._errors
        elif start > end:
            raise forms.ValidationError("Start date must not be past end date.")
        has_workgroups = len(cleaned_data.get("workgroups", [])) > 0
        if has_workgroups and cleaned_data.get("seminar") is not None:
            raise forms.ValidationError(
                "Please select either some workgroups or one seminar"
            )
        if has_workgroups:
            seminar = cleaned_data.get("workgroups")[0].seminar
            for w in cleaned_data.get("workgroups")[1:]:
                if w.seminar != seminar:
                    raise forms.ValidationError(
                        "All workgroups must be in the same seminar"
                    )
        return cleaned_data


class EventLinkInline(SeminarBoundModelMixin, admin.TabularInline):
    model = EventLink
    extra = 0
    ref = "event"

    def get_exclude(self, request, obj=None):
        if not request.user.has_perm("hide_eventlink_disclaimer"):
            return ["show_disclaimer"]

    # fieldsets = (
    #    (None, {
    #        'fields': ('name', 'url', 'new_tab', 'disabled')
    #    }),
    #    ('Access', {
    #        'fields': ('start_date', 'end_date')
    #    })
    # )


@admin.register(Event)
class EventAdmin(UsingSeminarBoundFieldMixin, SeminarBoundModelMixin, admin.ModelAdmin):
    form = EventForm
    inlines = [
        EventLinkInline,
    ]
    filter_horizontal = ["workgroups"]
    list_display = ["name", "start_date", "end_date", "disabled"]
    search_fields = ["name"]
    list_filter = ["disabled"]  # TODO get this to work with "enabled"

    ordering = ["start_date"]

    exclude = ["announced"]
    save_as = True

    # KKOWN BUG: When show_disclaimer is off and a user without hide_eventlink_disclaimer
    # changes the URL on an EventLink, then it is not turned back on

    def save_related(self, request, form, formsets, change):
        if change and not request.user.has_perm("hide_eventlink_disclaimer"):
            eventlink_formset = formsets[0]
            urlfield = EventLink.url.field.name
            for f in eventlink_formset.forms:
                if urlfield in f.changed_data:
                    url = f.cleaned_data[urlfield]
                    if not is_safe_url(url):
                        # slip a change of the model in before it is saved
                        # (this will reset a hidden disclaimer if the URL was changed by somebody who
                        # does not have permission to hide the disclaimer, preventing someone from
                        # putting a harmless URL, asking a superuser to disable the disclaimer, then
                        # changing the URL to something evil)
                        f.instance.show_disclaimer = True
        super().save_related(request, form, formsets, change)


#### ANNOUNCEMENTS ####
class AnnouncementForm(SeminarBoundProtectedModelForm):
    def clean(self):
        cleaned_data = super().clean()
        has_workgroups = len(cleaned_data.get("workgroups", [])) > 0
        if has_workgroups and cleaned_data.get("seminar") is not None:
            raise forms.ValidationError(
                "Please select either some workgroups or one seminar"
            )
        if has_workgroups:
            seminar = cleaned_data.get("workgroups")[0].seminar
            for w in cleaned_data.get("workgroups")[1:]:
                if w.seminar != seminar:
                    raise forms.ValidationError(
                        "All workgroups must be in the same seminar"
                    )
        return cleaned_data


@admin.register(Announcement)
class AnnouncementAdmin(
    UsingSeminarBoundFieldMixin, SeminarBoundModelMixin, admin.ModelAdmin
):
    form = AnnouncementForm
    filter_horizontal = ["workgroups"]
    list_display = ["name", "issue_date", "dismissed_count"]
    search_fields = ["name"]
    exclude = ["dismissed_by"]

    def save_model(self, request, obj, form, change):
        if change:
            obj.dismissed_by.clear()  # show again to all users
        super().save_model(request, obj, form, change)


#### Applications ####
@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass
