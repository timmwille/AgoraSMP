# Generated by Django 3.0.7 on 2020-06-24 12:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("usermgr", "0020_mailtoken_new_email"),
    ]

    operations = [
        migrations.AddField(
            model_name="expectedparticipant",
            name="registered",
            field=models.BooleanField(default=False),
        ),
    ]
