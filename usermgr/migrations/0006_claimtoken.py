# Generated by Django 3.0.6 on 2020-06-02 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("usermgr", "0005_workgroup"),
    ]

    operations = [
        migrations.CreateModel(
            name="Claimtoken",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("token", models.CharField(max_length=30, unique=True)),
                ("email", models.EmailField(max_length=254)),
                ("issued", models.DateTimeField()),
                ("expires", models.DateTimeField()),
            ],
        ),
    ]
