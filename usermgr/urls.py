# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.urls import path, include

from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    ### FRONTEND
    path("", views.IndexView.as_view(), name="index"),
    path("settings", views.AccountSettingsView.as_view(), name="account_settings"),
    path("claim/", views.ClaimAccountView.as_view(), name="claim"),
    path("claim/success", views.ClaimSuccessView.as_view(), name="claim_success"),
    path(
        "claim/token/<str:token_value>",
        views.ClaimSubmitTokenView.as_view(),
        name="claim_token",
    ),
    path(
        "change_email/token/<str:token_value>",
        views.SubmitMailTokenView.as_view(),
        name="email_token",
    ),
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="usermgr/auth.html"),
        name="login",
    ),
    path(
        "logout/",
        auth_views.LogoutView.as_view(template_name="usermgr/logout.html"),
        name="logout",
    ),
    path(
        "pw_reset/",
        auth_views.PasswordResetView.as_view(
            template_name="usermgr/pw_reset.html",
            form_class=views.LDAPPasswordResetForm,
        ),
        name="password_reset",
    ),
    path(
        "pw_reset/done",
        auth_views.PasswordResetDoneView.as_view(
            template_name="usermgr/pw_reset_done.html"
        ),
        name="password_reset_done",
    ),
    path(
        "pw_reset_request/<uidb64>/<token>/",
        views.LDAPPasswordResetConfirmView.as_view(
            template_name="usermgr/pw_reset_confirm.html",
            reset_url_token="set_password",
        ),
        name="password_reset_confirm",
    ),
    path(
        "pw_reset_request/done",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="usermgr/pw_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path("terms", views.TermsView.as_view(), name="terms"),
    path("delete_account", views.DeleteAccountView.as_view(), name="delete_account"),
    path("delete_success", views.DeleteSuccessView.as_view(), name="delete_success"),
    ### AJAX
    path("event_block", views.event_block, name="event_block"),
    path(
        "dismiss_announcement/<int:theID>",
        views.dismiss_announcement,
        name="dismiss_announcement",
    ),
]
