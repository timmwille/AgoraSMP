// Copyright (C) 2022 AgoraSMP Team
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
document.addEventListener("DOMContentLoaded", function() {
  const password_input = document.getElementById("password");
  const password_check_input = document.getElementById("password_check");
  const pwfields = [password_input, password_check_input];
  pwfields.forEach(e => e.addEventListener("change", ev => {
    if (password_input.value === "" || password_input.value === password_check_input.value) {
      pwfields.forEach(e => e.classList.remove("is-invalid"));
      password_check_input.setCustomValidity("");
    } else {
      pwfields.forEach(e => e.classList.add("is-invalid"));
      password_check_input.setCustomValidity("Passwords need to match");
    }
  }));
});