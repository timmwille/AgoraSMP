// Copyright (C) 2022 AgoraSMP Team
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
document.addEventListener("DOMContentLoaded", function() {
    const join_modal = document.getElementById("joinModal");
    join_modal.addEventListener('show.bs.modal', function (event) {
        let button = event.relatedTarget; // Button that triggered the modal
        let workgroup = button.dataset.workgroup; // Extract info from data-* attributes
        this.querySelector("#wgname").textContent = workgroup;
        this.querySelector("button.btn-primary").addEventListener('click', function(event) {
            // hot-swap the "show modal" button into a "submit form" button and click it
            button.setAttribute('type', 'submit');
            button.click();
        }, {once: true});
    });
});