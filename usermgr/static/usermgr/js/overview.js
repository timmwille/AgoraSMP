// Copyright (C) 2022 AgoraSMP Team
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, version 3.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
document.addEventListener("DOMContentLoaded", function() {
    const seminar_select = document.getElementById("seminarSelect");
    seminar_select.addEventListener('change', function(event) {
        document.querySelector("form#seminar-select-form").submit();
    });

    const announcement_dismiss_btns = Array.prototype.slice.call(document.getElementsByClassName("announcement-dismiss"));
    announcement_dismiss_btns.forEach(e => {
      e.addEventListener("click", async function(event) {
            let announcement_id = this.dataset.announcement;
            let only_one = (document.getElementsByClassName("announcement").length == 1);
            let token = getCookie('csrftoken');
            let resp = await fetch("/dismiss_announcement/" + announcement_id, {
                method: "POST",
                headers: {
                    "X-CSRFToken": token
                }
            });
            if (resp.ok || resp.status == 404) {
                if (only_one) {
                    document.getElementById("announcements").remove();
                } else {
                    this.parentElement.remove();
                }
            }
        });
    });

    const disclaimer_modal = document.getElementById("disclaimerModal");
    disclaimer_modal.addEventListener('show.bs.modal', function (event) {
        let button = event.relatedTarget; // Button that triggered the modal
        let link = button.dataset.link; // Extract info from data-* attributes
        this.querySelector("#disclaimer-link").textContent = link;
        this.querySelector("a.btn-primary").setAttribute('href', link);
    });
  });