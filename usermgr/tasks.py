import datetime

from celery import shared_task
from celery.utils.log import get_task_logger
from django.db.models import Q
from django.utils import timezone

from usermgr.models import Claimtoken, Event, Mailtoken, Seminar
from usermgr.signals.signals import *

logger = get_task_logger(__name__)

# periodic tasks triggered by Celery Beat
# these tasks are registered directly in settings.py


@shared_task(bind=True)
def prune_old_tokens(self):
    logger.info("pruning old tokens")
    Claimtoken.objects.all_invalid().delete()
    Mailtoken.objects.all_invalid().delete()


@shared_task(bind=True)
def advance_seminars(self):
    logger.info("processing seminar events")
    pending = Seminar.objects.all_with_pending_events()
    for sem in pending:
        state = sem.state
        if state == Seminar.States.CREATED:
            seminar_access_opens.send_robust(sender=self.__class__, seminar=sem)
            sem.state = Seminar.States.OPENED
            sem.save()
        elif state == Seminar.States.OPENED:
            seminar_begins.send_robust(sender=self.__class__, seminar=sem)
            sem.state = Seminar.States.RUNNING
            sem.save()
        elif state == Seminar.States.RUNNING:
            seminar_ends.send_robust(sender=self.__class__, seminar=sem)
            sem.state = Seminar.States.COMPLETED
            sem.save()
        elif state == Seminar.States.COMPLETED:
            seminar_access_closes.send_robust(sender=self.__class__, seminar=sem)
            sem.state = Seminar.States.CLOSED
            sem.save()
        elif state == Seminar.States.CLOSED:
            seminar_destroyed.send_robust(sender=self.__class__, seminar=sem)
            sem.state = Seminar.States.DESTROYED
            sem.save()


@shared_task(bind=True)
def signal_events(self):
    logger.info("signalling upcoming events")
    running = Seminar.objects.all_running()
    now = timezone.now()
    deadline = now + datetime.timedelta(minutes=5)
    upcoming_events = Event.objects.filter(
        disabled=False, announced=False, start_date__lt=deadline, end_date__gt=now
    )
    for sem in running:
        seminar_events = upcoming_events.filter(
            Q(seminar=sem) | Q(workgroups__seminar=sem)
        )
        for event in seminar_events:
            event_upcoming.send_robust(sender=self.__class__, event=event)
            event.announced = True
            event.save(update_fields=["announced"])
