# Copyright (C) 2022 AgoraSMP Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from datetime import timedelta

from bbbbutton.models import PersistentMeeting
from common import site, mail
from common.constants import PositionOptions
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordResetForm, _unicode_ci_compare
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import (
    password_validators_help_text_html,
    validate_password,
)
from django.contrib.auth.views import PasswordResetConfirmView
from django.core.exceptions import PermissionDenied, ValidationError
from django.db import transaction
from django.http import (
    HttpResponse,
    HttpResponseBadRequest,
    HttpResponseNotAllowed,
    HttpResponseNotFound,
    HttpResponseRedirect,
    JsonResponse,
)
from django.shortcuts import get_object_or_404, redirect, render
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils import dateparse, timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from usermgr.models import (
    Announcement,
    Claimtoken,
    DeletedUser,
    Event,
    ExpectedParticipant,
    Mailtoken,
    Seminar,
    Workgroup,
)

from . import token
from .ldapmanage.ldapconn import LDAPConnection
from .signals.signals import user_gdpr_delete

logger = logging.getLogger(__name__)
logger_auth = logging.getLogger("agorasmp.auth")

### FORMS ###


class EmailAddrForm(forms.Form):
    email = forms.EmailField(max_length=120)


def validate_name(value):
    # prevent some special characters in real name
    # (to discourage users from using fake names, or accidentally using their email address or password)
    invalid_chars = set(
        '@€!"$%&/=?+~#€<>|;:,.{[]}\\^°0123456789'
    )  # TODO are there more?
    if not set(value).isdisjoint(invalid_chars):
        raise ValidationError("Invalid characters: {}".format("".join(invalid_chars)))


class PersonFormMixin(forms.Form):
    given_name = forms.CharField(max_length=50, validators=[validate_name])
    surname = forms.CharField(max_length=50, validators=[validate_name])


class PasswordSetForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
    password_check = forms.CharField(widget=forms.PasswordInput)
    user = None

    def clean(self):
        cleaned_data = super().clean()
        a = cleaned_data.get("password")
        b = cleaned_data.get("password_check")
        if a is None or b is None:
            # ensure this has been noticed by the fields' validators
            # so we don't accidentally accept a None value
            assert self._errors
            return cleaned_data
        if a != b:
            self.add_error("password_check", "Passwords have to match")
        # TODO provide (synthetic) User object to make the similarity validator work
        validate_password(a, user=self.user)
        return cleaned_data


class ProfileForm(PersonFormMixin, EmailAddrForm):
    pass


class AccountRegisterForm(PersonFormMixin, PasswordSetForm):
    pass


class LDAPPasswordChangeForm(PasswordSetForm):
    old_password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        old_passwd = cleaned_data.get("old_password")
        passwd = cleaned_data.get("password")
        if old_passwd is None or passwd is None:
            assert (
                self._errors
            )  # ensure this has been noticed by the fields' validators
            # so we don't accidentally accept a None value
            return cleaned_data
        user = authenticate(username=self.user.username, password=old_passwd)
        if user is None:
            logger_auth.warning(
                "Incorrect password confirmation on password self-change for user %s",
                self.user.username,
            )
            self.add_error("old_password", "Your current password was incorrect")
            return
        if passwd == old_passwd:
            self.add_error("password", "Your new password can't be your old password")
        return cleaned_data


class LDAPPasswordResetForm(PasswordResetForm):
    # extend the PasswordResetView to force reset mails for LDAP users
    # (Django Auth suppresses reset mails for external authentication backends)

    # reimplement get_users from PasswordResetForm
    # but without the u.has_usable_password() check
    def get_users(self, email):
        userModel = get_user_model()
        email_field_name = userModel.get_email_field_name()
        active_users = userModel._default_manager.filter(
            **{
                "%s__iexact" % email_field_name: email,
                "is_active": True,
            }
        )
        print(active_users)
        return (
            u
            for u in active_users
            if _unicode_ci_compare(email, getattr(u, email_field_name))
        )


class PasswordRulesMixin:

    # populate "passwd_rules" context
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["passwd_rules"] = password_validators_help_text_html
        return context


class LDAPPasswordResetConfirmView(PasswordRulesMixin, PasswordResetConfirmView):
    # extend the PasswordResetConfirmView to push the new password into the LDAP
    # backend

    def form_valid(self, form):
        # if we don't control the password ourselves, then it's stored in LDAP,
        # so we need to update it

        if not self.user.has_usable_password():
            ad = LDAPConnection(
                settings.AUTH_LDAP_SERVER_URI,
                settings.AUTH_LDAP_BIND_DN,
                settings.AUTH_LDAP_BIND_PASSWORD,
            )
            ad.open()
            ad.user_passwd(self.user.username, form.cleaned_data["new_password1"])
        return super().form_valid(form)


class AccountDeactivateForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)
    agree = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user")
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        agree = cleaned_data.get("agree")
        if not agree:
            self.add_error(
                "agree",
                "You must check this field to confirm the deletion of your account",
            )
        password = cleaned_data.get("password")
        if password is None:
            assert (
                self._errors
            )  # ensure this has been noticed by the fields' validators
            # so we don't accidentally accept a None value
            return cleaned_data  # Missing field, can't continue checking
        user = authenticate(username=self.user.username, password=password)
        if user is None:
            self.add_error("password", "The password you entered was incorrect")
        return cleaned_data


### AJAX VIEWS ###


@login_required
@ensure_csrf_cookie
def dismiss_announcement(request, theID):
    if request.method != "POST":
        return HttpResponseNotAllowed(permitted_methods="POST")

    announcement = get_object_or_404(Announcement, pk=theID)

    # check access
    if announcement.seminar or announcement.workgroups.exists():
        profile = request.user.profile
        if announcement.seminar:
            seminar = announcement.seminar
            if not profile.workgroups.filter(seminar=seminar).exists():
                raise PermissionDenied()
        else:
            matching_wgs = profile.workgroups.filter(
                id__in=announcement.workgroups.values_list("id", flat=True)
            )
            if not matching_wgs.exists():
                raise PermissionDenied()
            # all workgroups are always in same seminar
            seminar = matching_wgs[0].seminar
        if not seminar.is_access_open():
            raise PermissionDenied()

    # save
    announcement.dismissed_by.add(request.user)
    return JsonResponse({"status": "ok"})


@login_required
def event_block(request):
    day = request.GET.get("date")
    if day is None:
        return HttpResponseBadRequest("Missing GET parameter date")
    date = dateparse.parse_date(day)

    profile = request.user.profile
    user_workgroups = profile.workgroups.select_related("seminar").all()
    seminars = set(map(lambda w: w.seminar, user_workgroups))
    slug = request.GET.get("seminar")
    selected_seminar = None
    if slug is None:
        return HttpResponseBadRequest("Missing GET parameter seminar")
    try:
        selected_seminar = Seminar.objects.get(slug=slug)
    except Seminar.DoesNotExist:
        return HttpResponseNotFound("Seminar does not exist")
    # stop if the user is not in this seminar or if the seminar
    # is no longer accessible
    if selected_seminar not in seminars or not selected_seminar.is_access_open():
        raise PermissionDenied()

    workgroups = selected_seminar.workgroup_set.filter(profile__pk=profile.pk)
    # TODO we currently load all events from DB, then filter in Django, but we should filter in DB directly if possible
    events = set(
        selected_seminar.event_set.filter(disabled=False)
        .select_related("meeting")
        .prefetch_related("eventlink_set")
    )  # use set to auto-deduplicate (since events may be in multiple workgroups)
    for workgroup in workgroups.prefetch_related("event_set", "event_set__meeting"):
        # this allows us to prefetch the event set, rather than query the filter on the DB un-prefetched
        es = filter(lambda e: not e.disabled, workgroup.event_set.all())
        events.update(es)
    event_days = list(_build_event_list(events, only_for_date=date))
    if not event_days:
        return JsonResponse({})
    template = loader.get_template("usermgr/event_entry.html")
    entries = []
    for event in event_days[0]["events"]:
        c = {"event": event}
        entry_html = template.render(c, request)
        entries.append(entry_html)
    return JsonResponse({"entries": entries})


### VIEWS ###


def _build_event_list(events, only_for_date=None):
    # TODO better if we could sort in DB
    now = timezone.now()
    today = now.astimezone().date()
    events = events.order_by("start_date").prefetch_related("eventlink_set")
    if events:
        min_date = only_for_date or min(map(lambda e: e.start_date.date(), events))
        max_date = only_for_date or max(map(lambda e: e.end_date.date(), events))
        date = min_date
        one_day = timezone.timedelta(days=1)
        while date <= max_date:
            events_that_day = list(filter(lambda e: e.contains_date(date), events))
            is_today = date == today
            event_items = []
            for e in events_that_day:
                is_past = e.end_date < now
                is_multiday = e.start_date.date() != e.end_date.date()
                buttons = e.eventlink_set.all()
                event_items.append(
                    {
                        "start": e.start_date,
                        "end": e.end_date,
                        "multiday": is_multiday,
                        "name": e.name,
                        "description": e.description,
                        "muted": is_past,
                        "meeting": e.meeting,
                        "buttons": buttons,
                    }
                )
            if event_items:
                yield {"date": date, "events": event_items, "today": is_today}
            date += one_day


@method_decorator(login_required, name="dispatch")
@method_decorator(ensure_csrf_cookie, name="dispatch")
class IndexView(TemplateView):
    template_name = "usermgr/overview.html"
    allowed_methods = ["get", "post"]

    def post(self, *args, **kwargs):
        slug = self.request.POST.get("s")
        if slug is not None:
            user = self.request.user
            profile = user.profile
            user_seminars = Seminar.objects.with_member(profile, accessible=True)
            try:
                selected_seminar = user_seminars.get(slug=slug)
                # we _could_ only do this when an update actually happens, but it's less readable
                # (Note that this will cause __autopick_seminar to not kick in on any future queries
                # unless the seminar is not available anymore, so users need to actively switch between
                # seminars. I currently believe this to be the more usable way anyway.)
                self.request.session["selected_seminar"] = selected_seminar.slug
            except Seminar.DoesNotExist:
                # if the user entered garbage, just handle it like
                # the user didn't enter anything
                logger.warning(
                    'User %s requested non-existent or non-accessible seminar "%s"',
                    self.request.user.username,
                    slug,
                )
        return HttpResponseRedirect(reverse("index"))

    def __autopick_seminar(self, seminars):
        now = timezone.now()
        # if the user did not select a seminar, try to auto-select the
        # latest seminar that is running
        active = (
            seminars.filter(start_date__lt=now, end_date__gt=now)
            .order_by("-start_date")
            .first()
        )
        if active is not None:
            return active
        # failing that, try the earliest seminar that is open
        # (that will be the first seminar that did not yet start
        # or, failing that, the first seminar that already ended)
        accessible = (
            seminars.filter(access_opens__lt=now, access_closes__gt=now)
            .order_by("access_opens")
            .first()
        )
        return accessible  # may be None if there is no seminar for this user

    def __make_seminar_selector_items(self, seminars, selected_slug):
        for seminar in filter(lambda s: s.is_access_open(), seminars):
            seminar_item = {
                "title": seminar.name,
                "slug": seminar.slug,
            }
            if selected_slug == seminar.slug:
                seminar_item["selected"] = True
            yield seminar_item

    def __make_future_seminars(self, seminars):
        return seminars.values_list("name", flat=True)

    def __make_sections(self, workgroups):
        # TODO localize these
        yield from [
            {"title": "My Timeline", "tag": "schedule"},
            {"title": "About this Seminar", "tag": "seminar-about"},
            {"title": "My Project Groups", "tag": "workgroups-about"},
        ]
        yield from [{"title": w.name, "tag": "heading-" + w.slug} for w in workgroups]

    def __make_event_schedule(self, seminar: Seminar, workgroups):
        events = Event.objects.for_seminar_view(seminar, workgroups)
        return list(_build_event_list(events))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        profile = user.profile
        user_seminars = Seminar.objects.with_member(profile, accessible=True)
        future_seminars = Seminar.objects.with_member(profile, future=True)

        selected_seminar = None
        slug_from_session = self.request.session.get("selected_seminar")
        if slug_from_session is not None:
            selected_seminar = user_seminars.filter(slug=slug_from_session).first()
        if selected_seminar is None:
            selected_seminar = self.__autopick_seminar(user_seminars)
            if selected_seminar is not None:
                self.request.session["selected_seminar"] = selected_seminar.slug
        if selected_seminar is not None:
            context["seminar_items"] = list(
                self.__make_seminar_selector_items(user_seminars, selected_seminar.slug)
            )

        context["navbar_items"] = site.build_nav(self.request, PositionOptions.HEADER)
        context["footer_items"] = site.build_nav(self.request, PositionOptions.FOOTER)
        context["future_seminars"] = list(self.__make_future_seminars(future_seminars))

        # If, in the future, you want to render some user-specific alerts, just put
        # them here as dicts (see /home/rene/git/agora/OpenAgora/usermgr/templates/usermgr/overview.html
        # for the fields you need)
        context["warnings"] = []

        if selected_seminar is not None:
            selected_workgroups = profile.workgroups.filter(
                seminar=selected_seminar
            ).all()
            context["seminar"] = selected_seminar
            context["applications"] = selected_seminar.applications.all()
            context["workgroups"] = selected_workgroups
            context["sections"] = list(self.__make_sections(selected_workgroups))
            context["event_days"] = list(
                self.__make_event_schedule(selected_seminar, selected_workgroups)
            )
            context["announcements"] = Announcement.objects.for_seminar_view(
                user, selected_seminar, selected_workgroups
            )
            context["meetings"] = PersistentMeeting.objects.for_seminar_view(
                user, selected_seminar, selected_workgroups
            )
        return context


class ProfileFormContext(site.FormContext):
    def construct_form(self, request, post, initial, prefix):
        return ProfileForm(post, initial=initial, prefix=prefix)

    def get_initial_data(self, request):
        return {
            "given_name": request.user.first_name,
            "surname": request.user.last_name,
            "email": request.user.email,
        }

    def form_action(self, request, profile_form):
        email = profile_form.cleaned_data["email"]
        given_name = profile_form.cleaned_data["given_name"]
        surname = profile_form.cleaned_data["surname"]
        if email != request.user.email:
            if (
                User.objects.filter(email=email).exists()
                or ExpectedParticipant.objects.filter(email=email)
                .filter(registered=False)
                .exists()
            ):
                profile_form.add_error("email", "This address is already being used.")
                logger_auth.warning(
                    'User %s attempted to set their email to the in-use address "%s"',
                    request.user.username,
                    email,
                )
                return False
            # send confirmation mail to change email address
            # (the user must thus confirm that they control that address so they can't
            # specify an invalid address or send mail to someone else)
            tkn_string = token.gentoken(30)
            now = timezone.now()
            offset = timedelta(days=1)
            expiry = now + offset
            tkn = Mailtoken(
                token=tkn_string,
                user=request.user,
                new_email=email,
                issued=now,
                expires=expiry,
            )
            # get seminar name
            seminar = (
                Seminar.objects.all_not_ended()
                .filter(workgroup__in=request.user.profile.workgroups.all())
                .order_by("start_date")
                .first()
            )
            seminar_title = settings.BRAND
            if seminar:
                seminar_title = seminar.name
            context = {
                "seminar_name": seminar_title,
                "user": request.user,
                "uri": request.build_absolute_uri(
                    reverse("email_token", kwargs={"token_value": tkn_string})
                ),
            }
            mail.send_templated_mail("confirm_email_change", [email], context)
            tkn.save()
            logger_auth.info(
                "User %s initiated email address change to %s",
                request.user.username,
                email,
            )
            messages.success(
                request, "Please check your inbox to confirm your new email address."
            )
        if given_name != request.user.first_name or surname != request.user.last_name:
            request.user.first_name = given_name
            request.user.last_name = surname
            request.user.save()
            logger_auth.info(
                "User %s changed their name to %s %s",
                request.user.username,
                given_name,
                surname,
            )
            messages.success(
                request,
                "Profile details updated. It can take up to one hour for your changes to take effect in all of your apps.",
            )
        return True


class LDAPPasswordChangeFormContext(site.FormContext):
    def construct_form(self, request, post, initial, prefix):
        return LDAPPasswordChangeForm(
            request.user, post, initial=initial, prefix=prefix
        )

    def form_action(self, request, password_form):
        password = password_form.cleaned_data["password"]
        if request.user.has_usable_password():
            request.user.set_password(password)
            logger_auth.warning(
                "Non-LDAP user %s changed their password", request.user.username
            )
            messages.success(
                request,
                "Your password has been changed. (Warning: You're a Non-LDAP user. Tell your admin in case this is unexpected.)",
            )
        else:
            ad = LDAPConnection(
                settings.AUTH_LDAP_SERVER_URI,
                settings.AUTH_LDAP_BIND_DN,
                settings.AUTH_LDAP_BIND_PASSWORD,
            )
            ad.open()
            ad.user_passwd(request.user.username, password)
            logger_auth.info("User %s changed their password", request.user.username)
            messages.success(
                request,
                "Your password has been changed. This password will take effect immediately in all of your apps.",
            )
        return True


@method_decorator(login_required, name="dispatch")
class AccountSettingsView(PasswordRulesMixin, site.MultiformView):
    template_name = "usermgr/settings.html"
    redirect_url = "account_settings"
    form_type_field = "form"

    form_contexts = {
        "profile_form": ProfileFormContext(),
        "pwchange_form": LDAPPasswordChangeFormContext(),
    }

    def __process_workgroup_modification(self, request):
        # we use POST to get nice URLs, no need to implement this as a Django form (the "form" has no fields)

        # TODO we might consider refactoring this into a form anyway, just to get
        # rid of this extra thing and the weird logic to override the default
        # POST behavior of MultiformView

        profile = request.user.profile
        join = "join" in request.POST
        if not join and "leave" not in request.POST:
            logger.warning(
                "User %s attempted invalid workgroup membership action",
                request.user.username,
            )
            messages.error(request, "Invalid workgroup membership action")
            return None

        slug = request.POST["join"] if join else request.POST["leave"]
        with transaction.atomic():
            try:
                # We get a lock on this workgroup so nobody else can enter this piece
                # of code, otherwise race conditions might happen and we could get
                # some users that manage to cheat the participant limit
                target_wg = Workgroup.objects.select_for_update().get(slug=slug)
            except Workgroup.DoesNotExist:
                logger.warning(
                    'User %s attempted to modify their membership in non-existent workgroup "%s"',
                    request.user.username,
                    slug,
                )
                messages.error(request, "Invalid workgroup membership action")
                return None

            user_seminars = Seminar.objects.all_accessible().filter(
                id__in=profile.workgroups.values_list("seminar", flat=True).distinct()
            )
            # user can't access that workgroup
            if not (
                user_seminars.filter(id=target_wg.seminar.id).exists()
                and join
                != request.user.profile.workgroups.filter(id=target_wg.id).exists()
            ):  # user requested join but is joined / leave and is not member
                logger.warning(
                    'User %s attempted to modify their membership in non-accessible workgroup "%s"',
                    request.user.username,
                    target_wg.slug,
                )
                return None

            if join:
                if not target_wg.users_can_join:
                    messages.error(request, "Invalid join or leave action.")
                    return None
                if target_wg.is_full():
                    messages.error(
                        request,
                        "Sorry, this workgroup has reached its participant limit. Someone else has possibly been quicker.",
                    )
                    return None
                profile.workgroups.add(target_wg)
                logger.info(
                    'User %s joined workgroup "%s"',
                    request.user.username,
                    target_wg.slug,
                )
                messages.success(request, "You're now a member of: " + target_wg.name)
                return redirect(self.redirect_url)
            else:
                # add user leaving logic here if ever needed
                messages.error(request, "Users leaving workgroups is not implemented")
                return None

    def dispatch(self, request, *args, **kwargs):
        resp = None
        if request.POST.get("extra_post") == "workgroupmanage":
            resp = self.__process_workgroup_modification(request)
        return resp or super().dispatch(request, *args, **kwargs)

    def __build_membership_manage_context(self):
        profile = self.request.user.profile
        user_seminars = (
            Seminar.objects.all_accessible()
            .order_by("start_date")
            .filter(
                id__in=profile.workgroups.values_list("seminar", flat=True).distinct()
            )
            .prefetch_related("workgroup_set")
        )
        user_workgroup_ids = list(
            profile.workgroups.values_list("id", flat=True)
        )  # we need those often, let's not hit the DB every time
        for seminar in user_seminars:
            wg_items = []
            for wg in seminar.workgroup_set.order_by("name").all():
                full = wg.is_full()
                wg_entry = {
                    "title": wg.name,
                    "slug": wg.slug,
                    "full": full,
                    "size_limited": wg.size_limited,
                    "participant_count": wg.get_member_count(),
                    "participant_limit": wg.participant_limit,
                }
                wg_entry["user_is_member"] = wg.id in user_workgroup_ids
                if wg_entry["user_is_member"]:
                    wg_entry[
                        "user_can_leave"
                    ] = False  # If we want to allow this later, set it here. Template is ready.
                else:
                    wg_entry["user_can_join"] = not full and wg.users_can_join
                wg_items.append(wg_entry)
            yield {
                "title": seminar.name,
                "slug": seminar.slug,
                "workgroups": wg_items,
                "running": seminar.is_active(),
            }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["seminar_items"] = list(self.__build_membership_manage_context())
        context["navbar_items"] = site.build_nav(self.request, PositionOptions.HEADER)
        context["footer_items"] = site.build_nav(self.request, PositionOptions.FOOTER)
        return context


@method_decorator(login_required, name="dispatch")
class SubmitMailTokenView(TemplateView):
    template_name = "usermgr/email_change.html"
    allowed_methods = ["get"]
    error = None

    def dispatch(self, request, *args, **kwargs):
        # not ideal, but I don't want to put this code which changes
        # DB entries into get_context_data
        token_value = self.kwargs.get("token_value")
        if token_value is None:
            return HttpResponse("Missing token.")
        try:
            mailtoken = Mailtoken.objects.get(token=token_value)
        except Mailtoken.DoesNotExist:
            self.error = "Token does not exist"
            logger_auth.warning(
                "Rejected email change using non-existent token %s", token_value
            )
        else:
            if not mailtoken.is_valid:
                self.error = "Token has expired. Request a new one."
                logger_auth.warning(
                    "User %s attempted to use expired mail token",
                    mailtoken.user.username,
                )
            elif not mailtoken.user == request.user:
                self.error = "Token is not valid for this user."
                logger_auth.warning(
                    "User %s attempted to use mail token for user %s",
                    request.user.username,
                    mailtoken.user.username,
                )
            elif (
                User.objects.filter(email=mailtoken.new_email).exists()
                or ExpectedParticipant.objects.filter(email=mailtoken.new_email)
                .filter(registered=False)
                .exists()
            ):
                self.error = (
                    "Another user has used this address in the meantime."
                )
                logger.warning(
                    "Mail token usage failed for address %s since another user has claimed that address since token was issued",
                    mailtoken.new_email,
                )
            else:
                user = mailtoken.user
                user.email = mailtoken.new_email
                user.save()  # triggers LDAP updater signal
                logger_auth.info(
                    "User %s completed email address change to %s",
                    user.username,
                    mailtoken.new_email,
                )
                mailtoken.delete()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["error"] = self.error
        return context


class ClaimAccountView(FormView):
    template_name = "usermgr/claim.html"
    form_class = EmailAddrForm
    success_url = reverse_lazy("claim_success")
    allowed_methods = ["get", "post"]

    def form_valid(self, form):
        email = form.cleaned_data["email"]

        try:
            participant = ExpectedParticipant.objects.get(email__iexact=email)
        except ExpectedParticipant.DoesNotExist:
            # TODO check if this is GDPR compliant if we give out this information
            # (this allows for anybody to check if we know this email address)
            form.add_error(
                "email", "This e-mail address is not on any participant list"
            )
            logger_auth.warning("Rejected account claim for unknown email %s", email)
            return super().form_invalid(form)

        if User.objects.filter(email__iexact=email).exists():
            form.add_error("email", "This e-mail address is already registered")
            logger_auth.warning("Rejected double claim for email %s", email)
            return super().form_invalid(form)

        tkn_string = token.gentoken(30)
        now = timezone.now()
        offset = timedelta(days=1)
        expiry = now + offset
        tkn = Claimtoken(
            token=tkn_string, participant=participant, issued=now, expires=expiry
        )
        # get seminar name
        seminar = (
            Seminar.objects.all_not_ended()
            .filter(workgroup__in=participant.workgroups.all())
            .order_by("start_date")
            .first()
        )
        seminar_title = settings.BRAND
        if seminar:
            seminar_title = seminar.name
        context = {
            "seminar_name": seminar_title,
            "user": participant,
            "uri": self.request.build_absolute_uri(
                reverse("claim_token", kwargs={"token_value": tkn_string})
            ),
            "index_uri": self.request.build_absolute_uri(reverse("index")),
        }
        mail.send_templated_mail("claim_your_account", [email], context)
        tkn.save()
        logger_auth.info(
            "Account claim initiated for participant %s", participant.username
        )
        return super().form_valid(form)


class ClaimSuccessView(TemplateView):
    template_name = "usermgr/claim_success.html"


class ClaimSubmitTokenView(FormView):
    template_name = "usermgr/claim_token.html"
    form_class = AccountRegisterForm
    success_url = reverse_lazy("index")
    allowed_methods = ["get", "post"]

    def dispatch(self, request, *args, **kwargs):
        self.token_value = self.kwargs.get("token_value")
        if self.token_value is None:
            return HttpResponse("Missing token.")
        try:
            self.token_object = Claimtoken.objects.get(token=self.token_value)
        except Claimtoken.DoesNotExist:
            logger_auth.warning(
                "Rejected account claim using non-existent token %s", self.token_value
            )
            return HttpResponse("Token does not exist")
        if not self.token_object.is_valid:
            logger_auth.warning(
                "Rejected attempt to use expired claim token %s", self.token_value
            )
            return HttpResponse("Token has expired. Request a new one.")
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        logger.info(
            "Claim token link opened by participant %s",
            self.token_object.participant.username,
        )
        return super().get(self, request, *args, **kwargs)

    def form_valid(self, form):
        participant = self.token_object.participant
        # final check
        if DeletedUser.objects.account_existed(participant.username):
            form.add_error(
                None,
                "There has been an issue with your username. Please contact an administrator and tell them you have an E_DEL_USER_CLASH.",
            )
            logger.error(
                "Account claim for %s failed due to clash with deleted user's username",
                participant.username,
            )
            return super().form_invalid(form)
        if get_user_model().objects.filter(username=participant.username).exists():
            form.add_error(
                None,
                "There has been an issue with your username. Please contact an administrator and tell them you have an E_REG_USER_CLASH.",
            )
            logger.error(
                "Account claim for %s failed due to clash with existing user's username",
                participant.username,
            )
            return super().form_invalid(form)
        # register the user in LDAP, then sign in immediately
        ad = LDAPConnection(
            settings.AUTH_LDAP_SERVER_URI,
            settings.AUTH_LDAP_BIND_DN,
            settings.AUTH_LDAP_BIND_PASSWORD,
        )
        ad.open()
        ad.user_create(
            participant.username,
            form.cleaned_data["given_name"],
            form.cleaned_data["surname"],
            participant.email,
            form.cleaned_data["password"],
        )
        # add user to group of activated users
        ad.group_join(
            settings.AUTH_LDAP_USER_FLAGS_BY_GROUP["is_active"],
            participant.username,
            gid_is_dn=True,
        )

        # TODO REMOVE THIS ONCE SERVICE SETTINGS ARE IMPLEMENTED
        for g in ["nextcloud", "mumble", "matrix"]:
            ad.group_join(g + "-users", participant.username)

        # force an authentication, this will pull the user profile into the local database from LDAP
        user = authenticate(
            self.request,
            username=participant.username,
            password=form.cleaned_data["password"],
        )
        # panic if the user didn't exist or LDAP thinks that the password is wrong (something's wrong with LDAP then)
        if user == None:
            logger.critical(
                "LDAP account not returned after creation of user %s",
                participant.username,
            )
            raise Exception("LDAP failure: created user does not exist")
        # now the user has been assigned an ID by the Django authentication app, so we can
        # save that ID to LDAP (some apps, like Mumble, need an unique numeric ID for each
        # user, and using the unique constraint on the User table is the easiest and most
        # reliable way to do this)
        ad.user_set_id(participant.username, user.pk)
        # assign the workgroup(s) from the expected participant list
        workgroups = participant.workgroups.all()
        profile = user.profile
        profile.workgroups.set(workgroups)
        profile.save()  # this will trigger the LDAP logic in signals.py to assign the matching LDAP groups
        # log that user in
        login(self.request, user)
        # mark the expected participant as registered
        # (this will (1) show the admins that the user has arrived and
        #  (2) prevent re-registration if the user changes their email
        #  and (3) allow a user who has changed their email to change it
        #  back to the original address)
        participant.registered = True
        participant.save()
        # invalidate the token
        self.token_object.delete()
        logger_auth.info("User registration completed for %s", user.username)
        return super().form_valid(form)

    def get_initial(self):
        initial = super().get_initial()
        participant = self.token_object.participant
        initial["given_name"] = participant.given_name
        initial["surname"] = participant.surname
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["token_value"] = self.token_value
        context["username"] = self.token_object.participant.username
        context["passwd_rules"] = password_validators_help_text_html
        return context


class TermsView(TemplateView):
    template_name = "usermgr/terms.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navbar_items"] = [
            {"url": reverse("terms"), "title": "Terms and Conditions", "active": True}
        ]
        return context


@method_decorator(login_required, name="dispatch")
class DeleteAccountView(FormView):
    template_name = "usermgr/delete_account.html"
    form_class = AccountDeactivateForm
    success_url = reverse_lazy("delete_success")
    allowed_methods = ["get", "post"]

    def dispatch(self, request, *args, **kwargs):
        logger.info("Account deletion form opened for user %s", request.user.username)
        if request.user.is_staff:  # staff can't delete themselves
            logger_auth.warning(
                "Rejected staff self-deletion attempt for %s", request.user.username
            )
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(DeleteAccountView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        name = self.request.user.username
        uid = self.request.user.id
        ad = LDAPConnection(
            settings.AUTH_LDAP_SERVER_URI,
            settings.AUTH_LDAP_BIND_DN,
            settings.AUTH_LDAP_BIND_PASSWORD,
        )
        ad.open()
        ## remove user from group of activated users
        # ad.group_kick(settings.AUTH_LDAP_USER_FLAGS_BY_GROUP['is_active'], request.user.username, gid_is_dn=True)
        # fully delete the user from LDAP
        ad.user_delete(name)
        # ask workers to wipe all data about that user
        user_gdpr_delete.send_robust(
            sender=self.form_valid, username=name, userid=uid
        )  # TODO check if sender is correct

        # ensure we remember this username (we store a hash value so no admin can see the user names (for GDPR),
        # but when someone registers we can still check if the name was ever used before)
        DeletedUser.from_username(name).save()
        logger_auth.info("Completed self-deletion for user %s", name)

        # forget the user and their profile
        self.request.user.delete()

        return super().form_valid(form)


class DeleteSuccessView(TemplateView):
    template_name = "usermgr/delete_success.html"


def error404(request, exception):
    return render(request, "usermgr/error404.html", status=404)


def error403(request, exception):
    return render(request, "usermgr/error403.html", status=403)


def error500(request):
    return render(request, "usermgr/error500.html", status=500)
